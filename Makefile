###############################################################################
# Makefile to build and run Cerberus Operating System                         #
# Nick Shoemaker                                                              #
#                                                                             #
# Because we are making a kernel we do not use the stdlib and we do           #
# our own linking. We are using ASM and c++ so we must also keep              #
# name mangling in mind as well.                                              #
###############################################################################

# Compiler options
CC=g++
CFLAGS=-Wall -Wextra -Wno-unused-variable -nostdlib -fno-builtin -nostartfiles -nodefaultlibs -fno-exceptions -fno-rtti -fno-stack-protector -m32 -c
LD=ld
LDFLAGS= -melf_i386
ASFLAGS= -felf32

# Find the linking files
LD_FILES = $(addprefix -T, $(shell find . -name "*.ld"))

# Find source files
S_FILES = $(shell find . -name "*.s")
CPP_FILES = $(shell find . -name "*.cpp")

# Object files
S_OBJ = $(addprefix $(BDIR),$(S_FILES:.s=.s.o))
CPP_OBJ = $(addprefix $(BDIR),$(CPP_FILES:.cpp=.cpp.o))

# Find direcories with header or source files
INC_DIRS = $(addprefix -I, $(shell find . -name "*.h" -printf "%h\n" | sort -u))
SRC_DIRS = $(addprefix $(BDIR), $(shell find -name "*.cpp" -printf "%h\n" -o -name "*.s" -printf "%h\n" | sort -u))

# The directory we will build into
BDIR =./build/
KERNEL = $(BDIR)kernel

# Emulator options
QEMU_IMAGE=qemu-system-x86_64 -fda $(BDIR)floppy.img
QEMU_KERNEL=qemu-system-x86_64 -kernel $(KERNEL)

# make build directories, compile to objects, like into kernel
all: make_dirs $(S_OBJ) $(CPP_OBJ) link
	@echo "Build Complete"

# Make doesn't like trying to build into a directory that doesn't exsist
# So make the build dirs first
make_dirs:
	@mkdir -p $(BDIR)
	@mkdir -p $(SRC_DIRS)

# Compile cpp into object files
$(CPP_OBJ): $(BDIR)%.cpp.o: %.cpp
	$(CC) $(INC_DIRS) $(CFLAGS) $^ -o $@

# Assemble ASM into object files
$(S_OBJ): $(BDIR)%.s.o: %.s
	nasm $(ASFLAGS) $^ -o $@

# Link object files with ld and our .ld
link:
	$(LD) $(LD_FILES) $(LDFLAGS) -o $(KERNEL) $(S_OBJ) $(CPP_OBJ)

# Run floppy image in qemu
run:
	@$(QEMU_IMAGE)

# Run kernel in qemu
runkernel:
	@$(QEMU_KERNEL)

# Delete build directory and everything in it
clean:
	@rm -R $(BDIR)

# floppy:
# 	grub-mkimage -p /boot -o build/floppy/core.img multiboot sh fat