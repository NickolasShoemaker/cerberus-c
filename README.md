###############################################################################
# Cerberus c++                                                                #
# Version alpha 0.1                                                           #
#                                                                             #
###############################################################################
Cerberus is aimed at getting as close to the microkernel exokernel 
architecture as possible which is a collection of moduals or OSLibs
that can be swaped in and out to change the functionality of the OS
without sacrificing stability or speed. The kernel will distribute 
resources to the OSLibs which will be responsible for them with no 
virtualization.

This project was created by the author for learning purposes. It is under the
MIT license. See ./license for more information.

# Contributers 2016                                                   #
Nickolas J Shoemaker

# Dependencies                                                        #
Reauires QEMU to run
Requires nasm, g++ to build

sudo apt-get install qemu nasm g++
