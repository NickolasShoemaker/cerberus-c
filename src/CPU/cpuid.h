/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* CPUID                                                                       *
* CPU id functionality for identifing cpu attributes                          *
*******************************************************************************/
#ifndef CPUID_H
#define CPUID_H

#include "types.h"

namespace cpuid{
	const uint32_t ECX_SSE3         = 1 << 0;
    const uint32_t ECX_PCLMUL       = 1 << 1;
    const uint32_t ECX_DTES64       = 1 << 2;
    const uint32_t ECX_MONITOR      = 1 << 3;
    const uint32_t ECX_DS_CPL       = 1 << 4;
    const uint32_t ECX_VMX          = 1 << 5;
    const uint32_t ECX_SMX          = 1 << 6;
    const uint32_t ECX_EST          = 1 << 7;
    const uint32_t ECX_TM2          = 1 << 8;
    const uint32_t ECX_SSSE3        = 1 << 9;
    const uint32_t ECX_CID          = 1 << 10;
    const uint32_t ECX_FMA          = 1 << 12;
    const uint32_t ECX_CX16         = 1 << 13;
    const uint32_t ECX_ETPRD        = 1 << 14;
    const uint32_t ECX_PDCM         = 1 << 15;
    const uint32_t ECX_DCA          = 1 << 18;
    const uint32_t ECX_SSE4_1       = 1 << 19;
    const uint32_t ECX_SSE4_2       = 1 << 20;
    const uint32_t ECX_x2APIC       = 1 << 21;
    const uint32_t ECX_MOVBE        = 1 << 22;
    const uint32_t ECX_POPCNT       = 1 << 23;
    const uint32_t ECX_AES          = 1 << 25;
    const uint32_t ECX_XSAVE        = 1 << 26;
    const uint32_t ECX_OSXSAVE      = 1 << 27;
    const uint32_t ECX_AVX          = 1 << 28;
 
    const uint32_t EDX_FPU          = 1 << 0;
    const uint32_t EDX_VME          = 1 << 1;
    const uint32_t EDX_DE           = 1 << 2;
    const uint32_t EDX_PSE          = 1 << 3;
    const uint32_t EDX_TSC          = 1 << 4;
    const uint32_t EDX_MSR          = 1 << 5;
    const uint32_t EDX_PAE          = 1 << 6;
    const uint32_t EDX_MCE          = 1 << 7;
    const uint32_t EDX_CX8          = 1 << 8;
    const uint32_t EDX_APIC         = 1 << 9;
    const uint32_t EDX_SEP          = 1 << 11;
    const uint32_t EDX_MTRR         = 1 << 12;
    const uint32_t EDX_PGE          = 1 << 13;
    const uint32_t EDX_MCA          = 1 << 14;
    const uint32_t EDX_CMOV         = 1 << 15;
    const uint32_t EDX_PAT          = 1 << 16;
    const uint32_t EDX_PSE36        = 1 << 17;
    const uint32_t EDX_PSN          = 1 << 18;
    const uint32_t EDX_CLF          = 1 << 19;
    const uint32_t EDX_DTES         = 1 << 21;
    const uint32_t EDX_ACPI         = 1 << 22;
    const uint32_t EDX_MMX          = 1 << 23;
    const uint32_t EDX_FXSR         = 1 << 24;
    const uint32_t EDX_SSE          = 1 << 25;
    const uint32_t EDX_SSE2         = 1 << 26;
    const uint32_t EDX_SS           = 1 << 27;
    const uint32_t EDX_HTT          = 1 << 28;
    const uint32_t EDX_TM1          = 1 << 29;
    const uint32_t EDX_IA64         = 1 << 30;
    const uint32_t EDX_PBE          = 1 << 31;
};

// Function prototypes for asm defined in utils.s
extern "C" bool check_cpuid();
//extern "C" uint32_t cpuid_get_vendor_string();

#endif //CPUID_H
