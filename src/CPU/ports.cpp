/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* Source file that defines sending information over ports                     *
*                                                                             *
* TODO: This could be moved directly into ASM for the most part.              *
* Something to consider as a lot of the project is in ASM anyway.             *
* This is so we can interface with C to send information to ports but         *
* as long as we are careful with calling we can call the ASM directly.        *
*******************************************************************************/
#include "ports.h"

extern "C" {
	/*
	* sends a uint8 to port as a byte
	*/
	void outb(uint16_t port, uint8_t value){
		asm volatile ("outb %1, %0" : : "dN" (port), "a" (value));
	}

	/*
	* sends a uint32 to port as a long
	*/
	void outl(uint16_t port, uint32_t value){
		asm volatile ("outl %1, %0" : : "dN" (port), "a" (value));
	}

	/*
	* retrieves a uint8 from port as a byte
	*/
	uint8_t inb(uint16_t port){
		uint8_t ret;
		asm volatile("inb %1, %0" : "=a" (ret) : "dN" (port));
		return ret;
	}

	/*
	* retrieves a uint 16 from port as a word
	*/
	uint16_t inw(uint16_t port){
		uint16_t ret;
		asm volatile ("inw %1, %0" : "=a" (ret) : "dN" (port));
		return ret;
	}

	/*
	* retrieves a uint32 from port as a long
	*/
	uint32_t inl(uint16_t port){
		uint32_t ret;
		asm volatile("inl %1, %0" : "=a" (ret) : "dN" (port));
		return ret;
	}
}
