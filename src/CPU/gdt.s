;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cerberus an Operating System written in c++                               ;;
;; Nick Shoemaker                                                            ;;
;;                                                                           ;;
;; Sets up Global Descriptor Table for the CPU                               ;;
;; This is for a 32 bit GDT we may have to make a different one if we want   ;;
;; a 64 bit OS.                                                              ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
; This is a Macro to write a byte to the GDT
;;
%macro GDT_ENTRY 5
	push eax
	push ebx
	push ecx
	mov eax, %1
	add eax, GDT32
	
	; here we set up the base
	mov ebx, %2 
	mov word [eax+2], bx
	shr ebx, 16
	mov byte [eax+4], bl
	mov byte [eax+7], bh
	
	; here we do the limit
	mov ebx, %3
	mov word   [eax], bx
	shr ebx, 16
	and  bl, 0fh
	mov byte [eax+6], bl
	
	; now we take care of the granularity
	mov bl, %5
	and bl, 0f0h
	or  byte [eax+6], bl
	
	; and finally the access byte
	mov byte [eax+5], %4
	
	
	pop ecx
	pop ebx
	pop eax

%endmacro

;;
; extern void idt_flush(uint32_t idt_pointer);
; loads idt
;;
[global idt_flush]
idt_flush:
   mov eax, [esp+4]  ; Get the pointer to the IDT, passed as a parameter. 
   lidt [eax]        ; Load the IDT pointer.
   ret

;;
; Here we load the GDT and TSS (32-bit)
;;
[global gdt32_install]
gdt32_install:
	GDT_ENTRY GDT32.Null, 0, 0, 0, 0
	GDT_ENTRY GDT32.Kernel_Code, 0, 0xFFFFFFFF, 0x9A, 0xCF
	GDT_ENTRY GDT32.Kernel_Data, 0, 0xFFFFFFFF, 0x92, 0xCF
	GDT_ENTRY GDT32.User_Code,   0, 0xFFFFFFFF, 0xFA, 0xCF
	GDT_ENTRY GDT32.User_Data,   0, 0xFFFFFFFF, 0xF2, 0xCF
	GDT_ENTRY GDT32.TSS, TSS32, TSS32+26*4, 0xE9, 0
	
	mov word [GDT32.Pointer], 6*8
	mov dword [GDT32.Pointer+2], GDT32
	
	lgdt [GDT32.Pointer]         ; Load 32-bit GDT
	mov ax, GDT32.Kernel_Data    
    mov ds, ax                   ; Load all data segment selectors
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    jmp 0x08: GDT32Loaded       ; Set the code segment and do a far jump!

;;
; called from gdt32_install
;;	
GDT32Loaded:
	call tss32_init
	ret


;;
; tss_32_init
; initialises the tss and tells the CPU where it is at
;;
[extern stack]
tss32_init:
	mov dword [TSS32 +  1*4], stack
	mov dword [TSS32 +  2*4], GDT32.Kernel_Data
	mov dword [TSS32 + 18*4], GDT32.Kernel_Data | 3
	mov dword [TSS32 + 19*4], GDT32.Kernel_Code | 3
	mov dword [TSS32 + 20*4], GDT32.Kernel_Data | 3
	mov dword [TSS32 + 21*4], GDT32.Kernel_Data | 3
	mov dword [TSS32 + 22*4], GDT32.Kernel_Data | 3
	mov dword [TSS32 + 23*4], GDT32.Kernel_Data | 3
	
	mov dword [TSS32 + 25*4], 0xFFFF0000
	mov ax, GDT32.TSS | 3
                   
    ltr ax
    ret

;;
; tss32_set_kernel_stack
; takes one argument, uint32_t. The value to move into esp0
;;
[global tss32_set_kernel_stack]
tss32_set_kernel_stack:
	push ebp
   	mov ebp, esp
	mov           eax, [esp+4]
	mov [TSS32 + 1*4], eax      ; Place of esp0 in the tss
	pop ebp
	retn

;;
; enter_user_mode
; Two param, server address, user_stack
;;
[extern user_stack]
[global enter_user_mode]
enter_user_mode:
	cli
    mov ax, 0x23
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov eax, [esp+8]
    push 0x23
    push eax
    pushf
    pop eax
    or eax, 0x200
    push eax
    push 0x1B
    mov eax, [esp + 20]
    push eax
    iret

;;
; The GDT
;;
section .bss
[global GDT32]
[global GDT32.Kernel_Code]
[global GDT32.Kernel_Data]
[global TSS32]
GDT32:
	.Null:        equ $ - GDT32  ; Our null descriptor, We have to have this or bad things happen
	resb 8
	
	.Kernel_Code:  equ $ - GDT32 ; The descriptor for our kernel code
	resb 8

	.Kernel_Data:  equ $ - GDT32 ; The descriptor for our kernel data
	resb 8

	.User_Code:    equ $ - GDT32  ; The descriptor for our user data
	resb 8

	.User_Data:    equ $ - GDT32  ; The descriptor for our user data
	resb 8
	
	.TSS:          equ $ - GDT32  ; The descriptor for our task segment selector
	resb 8
	
	.Pointer:                     ; The structure that we will tell the CPU to load the GDT
	resw 1                        ; Size of GDT32
	resd 1                        ; Pointer to GDT32

TSS32:
	resd 26 ;This structured is 104 bytes long
	
