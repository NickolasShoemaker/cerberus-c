;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cerberus an Operating System written in c++                               ;;
;; Nick Shoemaker                                                            ;;
;;                                                                           ;;
;; This file Sets up macros for the Iterrupts Descriptor Table               ;;
;;                                                                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
; This macro creates a stub for an ISR with no
; error code.
;;
%macro ISR_NOERRCODE 1
  global isr%1
  isr%1:
    cli                         ; Disable interrupts firstly.
    push byte 0                 ; Push a dummy error code.
    push  %1                    ; Push the interrupt number.
    jmp isr_common_stub         ; Go to our common handler code.
%endmacro

;;
; This macro creates a stub for an ISR which passes it's own
; error code.
;;
%macro ISR_ERRCODE 1
  global isr%1
  isr%1:
    cli                         ; Disable interrupts.
    push %1                     ; Push the interrupt number
    jmp isr_common_stub
%endmacro

;;
; This macro creates a stub for an IRQ - the first parameter is
; the IRQ number, the second is the ISR number it is remapped to.
;;
%macro IRQ 2
  global irq%1
  irq%1:
    cli
    push byte 0
    push byte %2
    jmp irq_common_stub
%endmacro


;;
; Set ISR Error codes
;; 
ISR_NOERRCODE 0
ISR_NOERRCODE 1
ISR_NOERRCODE 2
ISR_NOERRCODE 3
ISR_NOERRCODE 4
ISR_NOERRCODE 5
ISR_NOERRCODE 6
ISR_NOERRCODE 7
ISR_ERRCODE   8
ISR_NOERRCODE 9
ISR_ERRCODE   10
ISR_ERRCODE   11
ISR_ERRCODE   12
ISR_ERRCODE   13
ISR_ERRCODE   14
ISR_NOERRCODE 15
ISR_NOERRCODE 16
ISR_NOERRCODE 17
ISR_NOERRCODE 18
ISR_NOERRCODE 19
ISR_NOERRCODE 20
ISR_NOERRCODE 21
ISR_NOERRCODE 22
ISR_NOERRCODE 23
ISR_NOERRCODE 24
ISR_NOERRCODE 25
ISR_NOERRCODE 26
ISR_NOERRCODE 27
ISR_NOERRCODE 28
ISR_NOERRCODE 29
ISR_NOERRCODE 30
ISR_NOERRCODE 31
ISR_NOERRCODE 128

;;
; Set IRQ to ISR mappings
;; 
IRQ   1,    33
IRQ   2,    34
IRQ   3,    35
IRQ   4,    36
IRQ   5,    37
IRQ   6,    38
IRQ   7,    39
IRQ   8,    40
IRQ   9,    41
IRQ  10,    42
IRQ  11,    43
IRQ  12,    44
IRQ  13,    45
IRQ  14,    46
IRQ  15,    47


;; create symbols for external structures to use
[extern GDT32]
[extern GDT32.Kernel_Code]
[extern GDT32.Kernel_Data]
[extern TSS32]
; we have a special function for our timer (IRQ0)

;; prepare external function to be called in irq0
extern current_task
[extern schedule]

;;
; Special function for timer
;;
[global irq0]
irq0:
	cli
	;xchg bx, bx
	pusha
	
	push ds
	push es
	push fs
	push gs
	
	mov eax, GDT32.Kernel_Data
	mov ds, eax
	mov es, eax
	mov fs, eax
	mov gs, eax

	mov eax, cr3
	push eax

	mov  eax, esp
	push eax
	
	call schedule

	add  esp, 4
	
	mov esp, eax
	
	pop eax
	mov cr3, eax
	
	mov   al, 0x20
	out 0x20, al
	
	pop gs
	pop fs
	pop es
	pop ds
	
	popa
	iret

;; prepare external function to be called in isr_common_stub
extern isr 		; In isr.cpp

;;
; This is our common ISR stub. It saves the processor state, sets
; up for kernel mode segments, calls the C-level fault handler,
; and finally restores the stack frame.
;;
isr_common_stub:
    pusha                    ; Pushes edi,esi,ebp,esp,ebx,edx,ecx,eax
	
    mov ax, ds               ; Lower 16-bits of eax = ds.
    push eax                 ; save the data segment descriptor

    mov ax, GDT32.Kernel_Data  ; load the kernel data segment descriptor
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

	mov eax, esp
	push eax
    
    call isr       ; call isr from isr.cpp with regs in eax
	
	pop eax

    pop ebx        ; reload the original data segment descriptor
    mov ds, bx
    mov es, bx
    mov fs, bx
    mov gs, bx

    popa                     ; Pops edi,esi,ebp...
    add esp, 8     ; Cleans up the pushed error code and pushed ISR number
    sti
    iret           ; pops 5 things at once: CS, EIP, EFLAGS, SS, and ESP


;; prepare external function to be called in irq_common_stub
extern irq

;;
; This is our IRQ common stub. It calls the c level irq function
; to set up handlers
;;
irq_common_stub:
    pusha                    ; Pushes edi,esi,ebp,esp,ebx,edx,ecx,eax
	
    mov ax, ds               ; Lower 16-bits of eax = ds.
    push eax                 ; save the data segment descriptor

    mov ax, GDT32.Kernel_Data  ; load the kernel data segment descriptor
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

	mov eax, esp
	push eax
    
    call irq
	
	pop eax

    pop ebx        ; reload the original data segment descriptor
    mov ds, bx
    mov es, bx
    mov fs, bx
    mov gs, bx

    popa                     ; Pops edi,esi,ebp...
    add esp, 8     ; Cleans up the pushed error code and pushed ISR number
    sti
    iret           ; pops 5 things at once: CS, EIP, EFLAGS, SS, and ESP

%define BASE 0x100000

%define IDT32_ENTRY_SIZE 8
%define IDT32_LENGTH IDT32_ENTRY_SIZE * 256

;;
; Macro to set IDT Entry
;;
%macro IDT_ENTRY 4
	push eax
	push ebx
	push ecx

	; Entry index
	mov eax, %1
	lea eax, [eax * 8]
	add eax, IDT32

	; Label for handler
	mov ebx, %2 
	mov word [eax], bx

	; Set selector
	mov word [eax+2], %3
	mov byte [eax+4], 0

	; Set flags
	mov byte [eax+5], %4 ;| 0x60
	shr ebx, 10h
	mov word [eax+6], bx
	
	pop ecx
	pop ebx
	pop eax

%endmacro

;;
; Instal IDT (32 bit)
;;
[global idt32_install]
idt32_install:
	
  	xor eax, eax
	mov   al,  11h
	out 020h,   al
	
	mov   al,  11h
	out 0A0h,   al
	
	mov   al,  20h
	out 021h,   al
	
	mov   al,  28h
	out 0A1h,   al
	
	mov   al,  04h
  	out 021h,   al
  	
  	mov   al,  02h
  	out 0A1h,   al
  	
  	mov   al,  01h
  	out 021h,   al
  	
  	mov   al,  01h
  	out 0A1h,   al
  	
  	mov   al,  00h
  	out 021h,   al
  	
  	mov   al,  00h
  	out 0A1h,   al
	
	mov ecx, 256 * 2
	mov eax, IDT32
	
	.loop:
		mov dword [eax], 0
		add eax, 4
		dec ecx
		cmp ecx, 0
		jnz .loop
		
	;;
	; Set IDT Entries using Macro
	;;
	IDT_ENTRY 0, isr0, 0x08, 0x8E
	IDT_ENTRY 1, isr1, 0x08, 0x8E
	IDT_ENTRY 2, isr2, 0x08, 0x8E
	IDT_ENTRY 3, isr3, 0x08, 0x8E
	IDT_ENTRY 4, isr4, 0x08, 0x8E
	IDT_ENTRY 5, isr5, 0x08, 0x8E
	IDT_ENTRY 6, isr6, 0x08, 0x8E
	IDT_ENTRY 7, isr7, 0x08, 0x8E
	IDT_ENTRY 8, isr8, 0x08, 0x8E
	IDT_ENTRY 9, isr9, 0x08, 0x8E
	IDT_ENTRY 10, isr10, 0x08, 0x8E
	IDT_ENTRY 11, isr11, 0x08, 0x8E
	IDT_ENTRY 12, isr12, 0x08, 0x8E
	IDT_ENTRY 13, isr13, 0x08, 0x8E
	IDT_ENTRY 14, isr14, 0x08, 0x8E
	IDT_ENTRY 15, isr15, 0x08, 0x8E
	IDT_ENTRY 16, isr16, 0x08, 0x8E
	IDT_ENTRY 17, isr17, 0x08, 0x8E
	IDT_ENTRY 18, isr18, 0x08, 0x8E
	IDT_ENTRY 19, isr19, 0x08, 0x8E
	IDT_ENTRY 20, isr20, 0x08, 0x8E
	IDT_ENTRY 21, isr21, 0x08, 0x8E
	IDT_ENTRY 22, isr22, 0x08, 0x8E
	IDT_ENTRY 23, isr23, 0x08, 0x8E
	IDT_ENTRY 24, isr24, 0x08, 0x8E
	IDT_ENTRY 25, isr25, 0x08, 0x8E
	IDT_ENTRY 26, isr26, 0x08, 0x8E
	IDT_ENTRY 27, isr27, 0x08, 0x8E
	IDT_ENTRY 28, isr28, 0x08, 0x8E
	IDT_ENTRY 29, isr29, 0x08, 0x8E
	IDT_ENTRY 30, isr30, 0x08, 0x8E
	IDT_ENTRY 31, isr31, 0x08, 0x8E
	IDT_ENTRY 32, irq0,  0x08, 0x8E
	IDT_ENTRY 33, irq1,  0x08, 0x8E
	IDT_ENTRY 34, irq2,  0x08, 0x8E
	IDT_ENTRY 35, irq3,  0x08, 0x8E
	IDT_ENTRY 36, irq4,  0x08, 0x8E
	IDT_ENTRY 37, irq5,  0x08, 0x8E
	IDT_ENTRY 38, irq6,  0x08, 0x8E
	IDT_ENTRY 39, irq7,  0x08, 0x8E
	IDT_ENTRY 40, irq8,  0x08, 0x8E
	IDT_ENTRY 41, irq9,  0x08, 0x8E
	IDT_ENTRY 42, irq10, 0x08, 0x8E
	IDT_ENTRY 43, irq11, 0x08, 0x8E
	IDT_ENTRY 44, irq12, 0x08, 0x8E
	IDT_ENTRY 45, irq13, 0x08, 0x8E
	IDT_ENTRY 46, irq14, 0x08, 0x8E
	IDT_ENTRY 47, irq15, 0x08, 0x8E
	IDT_ENTRY 128, isr128, 0x08, 0x8E
	
	mov word [IDT32.Pointer], 256*8
	mov dword [IDT32.Pointer+2], IDT32
	
	lidt [IDT32.Pointer]
	ret


section .bss

;;
; The IDT
;;
global IDT32
IDT32:
	resb IDT32_LENGTH
	
	.Pointer:      ; The structure that we will tell the CPU to load the IDT
	resw 1
	resd 1
