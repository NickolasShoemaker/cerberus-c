/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file declares the Interrupt Service Routines functionality             *
*                                                                             *
*******************************************************************************/
#ifndef ISR_H
#define ISR_H

#include "types.h"
#include "ports.h"
#include "utils.h"

typedef void (*isr_t)(registers_t*);

namespace interrupts{
	//prototypes
	void add_handler(uint8_t, isr_t);
	void enable();
	void disable();
};

#endif //ISR_H
