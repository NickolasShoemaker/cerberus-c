/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* CPUID                                                                       *
* CPU id functionality for identifing cpu attributes                          *
*******************************************************************************/
#include "cpuid.h"