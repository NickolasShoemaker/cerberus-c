/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines the Interrupt Service Routines functionality              *
*                                                                             *
*******************************************************************************/
#include "isr.h"

#define MAX_HANDLERS 8

uint32_t interrupt_counts[256];
isr_t interrupt_handlers[256][MAX_HANDLERS];

namespace interrupts{
	/*
	* Associate a certain interrupt handler with an
	* interrupt number
	*/
	void add_handler(uint8_t n,isr_t handler){
		for(uint8_t i = 0; i < MAX_HANDLERS; i++){
			if(interrupt_handlers[n][i] == 0){
				interrupt_handlers[n][i] = handler;
				break;
			}
		}
	}

	/*
	* Send command to CPU to enable interrupts
	*/
	void enable(){
		asm volatile("sti");
	}

	/*
	* Send command to CPU to disable interrupts
	*/
	void disable(){
		asm volatile("cli");
	}
}

extern "C"{
	/*
	* This gets called from idt.s
	* Set interrupt handlers for Interrupt Description Table
	*/
	void isr(registers_t* regs){
		for(uint8_t i = 0; i < MAX_HANDLERS; i++)
			if(interrupt_handlers[regs->int_no][i] != NULL){
	        	isr_t handler = interrupt_handlers[regs->int_no][i];
	        	handler(regs);
	    	}
	    	
	    interrupt_counts[regs->int_no]++;
	}

	/*
	* This gets called from idt.s
	* Set interrupt handlers for Interrupt Description Table
	* and the IRQ's they are mapped to
	*/
	void irq(registers_t* regs){
	   if(regs->int_no >= 40)
	       outb(0xA0, 0x20);
	       
	   outb(0x20, 0x20);
	   
	   for(uint8_t i = 0; i < MAX_HANDLERS; i++)
			if(interrupt_handlers[regs->int_no][i] != NULL){
	        	isr_t handler = interrupt_handlers[regs->int_no][i];
	        	handler(regs);
	        }
	        
	   interrupt_counts[regs->int_no]++;
	}
};
