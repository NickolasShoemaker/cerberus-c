/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines the Interrupts Descriptor Table                           *
*                                                                             *
*******************************************************************************/
#ifndef IDT_H
#define IDT_H

#include "types.h"
#include "ports.h"
#include "utils.h"

// Entry point for interrupts
struct idt_entry {
   uint16_t base_lo;             // The lower 16 bits of the address to jump to when this interrupt fires.
   uint16_t sel;                 // Kernel segment selector.
   uint8_t  always0;             // This must always be zero.
   uint8_t  flags;               // More flags. See documentation.
   uint16_t base_hi;             // The upper 16 bits of the address to jump to.
} __attribute__((packed));
typedef struct idt_entry idt_entry_t;

// Pointer to Interrupts Descriptor Table
struct idt_pointer {
	uint16_t limit;
   	uint32_t base;  

} __attribute__((packed));
typedef struct idt_pointer idt_pointer_t;

/*
* Interupt handlers
*/
extern "C"{
	// isr Function prototypes defined in idt.s
	void isr0 ();
	void isr1 ();
	void isr2 ();
	void isr3 ();
	void isr4 ();
	void isr5 ();
	void isr6 ();
	void isr7 ();
	void isr8 ();
	void isr9 ();
	void isr10();
	void isr11();
	void isr12();
	void isr13();
	void isr14();
	void isr15();
	void isr16();
	void isr17();
	void isr18();
	void isr19();
	void isr20();
	void isr21();
	void isr22();
	void isr23();
	void isr24();
	void isr25();
	void isr26();
	void isr27();
	void isr28();
	void isr29();
	void isr30();
	void isr31();

	// irq Function prototypes defined in idt.s
	void irq0 ();
	void irq1 ();
	void irq2 ();
	void irq3 ();
	void irq4 ();
	void irq5 ();
	void irq6 ();
	void irq7 ();
	void irq8 ();
	void irq9 ();
	void irq10();
	void irq11();
	void irq12();
	void irq13();
	void irq14();
	void irq15();
};

#endif //IDT_H
