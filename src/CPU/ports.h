/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* header that defines ports of the CPU                                        *
*******************************************************************************/
#ifndef PORTS_H
#define PORTS_H

#include "types.h"

extern "C" {
	// Out put to ports
	void outb(uint16_t, uint8_t);
	void outl(uint16_t, uint32_t);

	// Read in from ports
	uint8_t inb(uint16_t);
	uint16_t inw(uint16_t);
	uint32_t inl(uint16_t);
}

#endif //PORTS_H