/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines basic video output from kernel space.                     *
*******************************************************************************/
#include "video.h"

namespace video{
	/*
	* cursor position
	*/
	uint16_t cursor_x;
	uint16_t cursor_y;

	/*
	* screen size
	*/
	uint16_t screen_x;
	uint16_t screen_y;

	/*
	* output attribute
	*/
	uint8_t attr;

	/*
	* text buffer
	*/
	uint16_t* text_fb;

	/*
	* set window size, cursor pos, attribute code, and text buffer.
	*/
	void init(){
		text_fb = (unsigned short*)0xb8000;	//get pointer to text buffer
		screen_x = 80;
		screen_y = 24;
		cursor_x = 0;
		cursor_y = 0;
		attr = 0x07;
	}
	/*
	* print null terminated constant string
	*/
	void puts(const char* s){
		while(*s){
			putch(*s);
			s++;
		}
	}
	/*
	* print null terminated string
	*/
	void puts(char* s){
		while(*s){
			putch(*s);
			s++;
		}
	}
	/*
	* clear the screan completely
	*/
	void clear(){
		for(uint16_t i = 0; i < screen_x * screen_y; i++){ // 80*24
			text_fb[i] = ' ' | (attr << 8);
		}
	}
	/*
	* output as hex
	*/
	void hex(uint32_t n){
		puts(convert(n,16));

	}
	/*
	* output as decimal
	*/
	void dec(uint32_t n){
		puts(convert(n,10));

	}
	/*
	* everything devolves to putting characters out one at a time
	*/
	void putch(char c){
		switch(c) {
			case '\b':
				if(cursor_x != 0){
					cursor_x--;
				}else{
					cursor_y--;
					cursor_x = screen_x - 1;
				}
				text_fb[cursor_x+screen_x * cursor_y] = ' ' | (attr << 8);
				break;
			case '\r':
				cursor_x = 0;
				break;
			case '\n':
				cursor_y++;
				cursor_x = 0;
				break;
			case '\t':
				putch(' ');
				putch(' ');
				putch(' ');
				putch(' ');
				break;
			default:
				if(c >= ' '){
					text_fb[cursor_x + screen_x * cursor_y] = c | (attr << 8);
					cursor_x++;
					if(cursor_x >= screen_x){ // cursor has reached end of screen go to new line
						cursor_y++;
						cursor_x = 0;
					}
				}
				break;
		}
		if(cursor_y >= screen_y){
			scroll();
			cursor_y = screen_y - 1;	//scroll again next line
		}	
		update_cursor();
	}

	/*
	* change attributes of output
	*/
	void set_attributes(uint8_t fore, uint8_t back){
		attr = ((back << 4) & 0x0f) | (fore & 0x0f);
	}
	void set_attributes(uint8_t col){
		attr = col;
	}
	uint8_t get_attributes(){
		return attr;
	}
	uint16_t get_cursor_x(){
		return cursor_x;
	}
	uint16_t get_cursor_y(){
		return cursor_y;
	}
	void set_cursor_x(uint16_t cur_x){
		cursor_x = cur_x;
	}
	void set_cursor_y(uint16_t cur_y){
		cursor_y = cur_y;
	}

	/*
	* Where the magic happens
	*/
	void update_cursor(){
		outb(0x3D4, 0xe);                  
	   	outb(0x3D5, (cursor_x+screen_x * cursor_y) >> 8);
	   	outb(0x3D4, 0xf);                  
	   	outb(0x3D5, (cursor_x+screen_x * cursor_y) & 0xFF);
	}

	/*
	* move down when screen is full
	*/
	void scroll(){
		uint32_t i, j;
		for(i = 0; i < screen_y - (unsigned)1; i++){
			for(j = 0; j < screen_x; j++){
				text_fb[j + screen_x * i] = text_fb[j + screen_x * (i+1)];	//set each row equal to next
			}	
		}
		for(i = 0; i < screen_x; i++){
			text_fb[i + screen_x * (screen_y - 1)] = (attr << 8) | ' ';	//blank out bottom row
		}
	}
};