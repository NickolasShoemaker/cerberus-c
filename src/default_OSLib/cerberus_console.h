/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file declares the output stream used internaly by the kernel.          *
*                                                                             *
*******************************************************************************/
#ifndef cerberus_console_h
#define cerberus_console_h

#include "types.h"
#include "video.h"
#include "utils.h"

namespace internal_stream{
	/*
	* class for color objects in kernel space output
	*/
	class cerberus_console_color{
	public:
		uint8_t color;
		cerberus_console_color(uint8_t, uint8_t);
	};

	/*
	* class for kernel space output
	*/
	class cerberus_console{
		uint32_t args;
		uint8_t attr;
	public:
		cerberus_console();
		cerberus_console(uint8_t, uint8_t);
		
		void flush(uint8_t, bool=false);

		cerberus_console& operator <<(char*);
		cerberus_console& operator <<(const char*);
		cerberus_console& operator <<(char);
		cerberus_console& operator <<(uint32_t);
		cerberus_console& operator <<(int32_t);
		cerberus_console& operator <<(float);
		cerberus_console& operator <<(cerberus_console_color);
	};

	/*
	* make kernel output stream static object
	*/
	static cerberus_console kout;
	static cerberus_console kerror(VGA_RED, VGA_BLACK);
};

#endif //cerberus_console_h
