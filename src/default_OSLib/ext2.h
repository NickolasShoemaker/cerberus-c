/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines information about the ext2 file system.                   *
*******************************************************************************/
#ifndef EXT2_H
#define EXT2_H

#include "types.h"

#define EXT2_SIGNATURE 0xEF53

namespace ext2{
	/*
	* Is there an error we need to handle
	*/
	enum super_block::filesystem_state_type: uint16_t{
		clean = 1,
		error = 2
	};

	/*
	* How should we handle an error
	*/
	enum super_block::error_handle_type: uint16_t{
		ignore = 1,
		remount_read_only = 2,
		kernel_panic = 3
	};

	/*
	* What os goes with this fs
	*/
	enum super_block::os_id_type: uint16_t{
		linux = 0,
		gnu_hurd = 1,
		masix = 2,
		freebsd = 3,
		bsdlite = 4
	};

	/*
	* The super block is used to travers the filesystem
	*/
	struct super_block{
		uint32_t			num_inodes;
		uint32_t			num_blocks;
		uint32_t			num_superuser_blocks;
		uint32_t			num_unallocated_blocks;
		uint32_t			num_unallocated_inodes;
		uint32_t			num_superblock;
		uint32_t			block_size;
		uint32_t			fragment_size;
		uint32_t			num_blocks_per_block_group;
		uint32_t			num_fragments_per_block_group;
		uint32_t			num_inodes_per_block_group;
		uint32_t			posix_last_mount_time;
		uint32_t			posix_last_write_time;
		uint16_t			num_mounts_since_check;
		uint16_t			num_mounts_before_check;
		uint16_t			signature;
		filesystem_type 	filesystem_state;
		error_handle_type 	handle_error;
		uint16_t 			minor_version;
		uint32_t 			posix_check_time;
		uint32_t 			posix_time_between_force_check;
		os_id_type 			operating_system_id;
		uint32_t 			major_version;
		uint16_t 			user_id;
		uint16_t 			group_id;
	}__attribute__((packed));

	/*
	* Additional information for the super block
	*/
	struct extended_super_block{
		uint32_t 			first_inode;
		uint16_t 			inode_size_bytes;
		uint16_t 			block_group;
		uint32_t 			optional_features_present;
		uint32_t 			required_features_present;
		uint32_t 			read_only_features;
		char 				filesystem_id[16];
		char 				null_terminated_volume_name[16];
		char 				null_terminated_last_mount_point[64];
		uint32_t 			compression_algorithms;
		uint8_t 			file_blocks_to_preallocate;
		uint8_t 			directory_blocks_to_preallocate;
		uint16_t 			reserved;
		char 				journal_id[16];
		uint32_t 			journal_inode;
		uint32_t 			journal_device;
		uint32_t 			head_orphane_inode_list;
		uint8_t 			reserved_end[787];
	}__attribute__((packed));

	/*
	* A file or directory entry in the fs tree
	*/
	struct inode{
		uint16_t 			permisions;
		uint16_t 			user_id;
		uint32_t 			lower_32_bites_size;
		uint32_t 			posix_last_access_time;
		uint32_t 			posix_creation_time;
		uint32_t 			posix_modification_time;
		uint32_t 			posix_deletion_time;
		uint16_t 			group_id;
		uint16_t 			num_hard_links;
		uint32_t 			num_disk_sectors;
		uint32_t 			flags;
		uint32_t 			first_os_specific_value;
		uint32_t 			block_pointer_0;
		uint32_t 			block_pointer_1;
		uint32_t 			block_pointer_2;
		uint32_t 			block_pointer_3;
		uint32_t 			block_pointer_4;
		uint32_t 			block_pointer_5;
		uint32_t 			block_pointer_6;
		uint32_t 			block_pointer_7;
		uint32_t 			block_pointer_8;
		uint32_t 			block_pointer_9;
		uint32_t 			block_pointer_10;
		uint32_t 			block_pointer_11;
		uint32_t 			singly_indirect_block_pointer;
		uint32_t 			doubly_indirect_block_pointer;
		uint32_t 			triply_indirect_block_pointer;
		uint32_t 			generation_number;
		uint32_t 			extended_attribute_block;
		uint32_t 			upper_32_bites_size;
		uint32_t 			fragment_block_address;
		uint32_t 			second_os_specific_value[6];
	}__attribute__((packed));

	/*
	* Information about blocks
	*/
	struct block_descriptor_table{
		uint32_t 			block_usage_bitmap;
		uint32_t 			inode_usage_bitmap;
		uint32_t 			inode_table;
		uint16_t 			num_unallocated_blocks;
		uint16_t 			num_unallocated_inodes;
		uint16_t 			num_directories;
		uint16_t 			reserved;
	}__attribute__((packed));

	void init();
	int read(char*,int);
	int write(char*,int);
	void create_new_file(char*);
	void create_new_directory(char*);
	void move(char*,char*);
	void copy(char*,char*);
	void delete(char*);
	void list(char*);
}

#endif //EXT2_H