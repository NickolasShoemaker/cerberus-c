/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file handles the loading of the Executable and Linking Format.         *
*                                                                             *
*******************************************************************************/
#ifndef ELF_H
#define ELF_H

#include "types.h"

namespace elf{
	/*
	* The header of an elf file
	*/
	template<typename Type>
	struct elf_header{
		uint8_t 	magic;
		char 		ELF_askii[3];
		uint8_t 	bits;
		uint8_t 	endian;
		uint8_t 	version;
		uint8_t 	OS_ABI;
		uint64_t 	padding;
		uint16_t 	functionality;
		uint16_t 	instruction_set;
		uint32_t 	elf_version;
		Type 	 	program_entry;
		Type 	 	program_header_table;
		Type 	 	section_header_table;
		uint32_t 	flags;
		uint16_t 	header_size;
		uint16_t 	program_header_table_entry_size;
		uint16_t 	program_header_table_size;
		uint16_t 	section_header_table_entry_size;
		uint16_t 	section_header_table_size;
		uint16_t	section_header_index_names;
	}__attribute__((packed));

	/*
	* 32 and 64 bit versions
	*/
	using elf_header_32 = elf_header<uint32_t>;
	using elf_header_64 = elf_header<uint64_t>;

	/*
	* The type of segment
	*/
	enum segment_type: uint32_t{
		ignor = 0,
		load = 1,
		dynamic = 2,
		interpreted = 3,
		note = 4
	};

	/*
	* Header of an elf program
	*/
	template<typename Type>
	struct program_header{
		segment_type	type;
		uint32_t		data_offset;
		Type			virtual_addr_start;
		Type			padding;
		Type			file_size;
		Type			mem_size;
		Type			flags;
		Type			alignement;
	}__attribute__((packed));

	/*
	* 32 and 64 bit versions
	*/
	using program_header_32 = program_header<uint32_t>;
	using program_header_64 = program_header<uint64_t>;

	/*
	* The type of section
	*/
	enum elf_section_header::types{
		null		= 0,   // null section
		progbits	= 1,   // program information
		symtab		= 2,   // symbol table
		strtab		= 3,   // string table
		rela		= 4,   // relocation (w/ addend)
		nobits		= 8,   // not present in file
		rel			= 9,   // relocation (no addend)
	};
	
	/*
	* The section attributes
	*/ 
	enum elf_section_header::attributes{
		write	= 0x01, // writable section
		alloc	= 0x02  // Exists in memory
	};

	/*
	* Section header
	*/
	template<typename Type>
	struct elf_section_header{
		Type	name;
		types	type;
		Type	flags;
		Type	addr;
		Type 	offset;
		Type	size;
		Type	link;
		Type	info;
		Type	addr_align;
		Type	entry_size;
	};

	/*
	* 32 and 64 bit versions
	*/
	using elf_section_header_32 = elf_section_header<uint32_t>;
	using elf_section_header_64 = elf_section_header<uint64_t>;

	/*
	* symbol table scopes
	*/
	enum symbol_table::bindings{
		local_scope 	= 0,
		global_scope 	= 1,
		weak 			= 2,
	};

	/*
	* symbol table types
	*/
	enum symbol_table::type{
		no_type 	= 0,
		object 		= 1,
		function 	= 2
	};

	/*
	* binding and type info codes
	*/
	struct symbol_table::info_t: uint8_t{
		binding: 	4;
		type:		4;
	};

	/*
	* Symbol table
	*/
	template<typename Type>
	struct symbol_table{
		Type name;
		Type value;
		Type size;
		info_t	 info;
		uint8_t	 other;
		uint16_t index;
	};

	/*
	* 32 and 64 bit versions
	*/
	using symbol_table_32 = symbol_table<uint32_t>;
	using symbol_table_64 = symbol_table<uint64_t>;

	/*
	* Function to load an elf file
	*/
	void load(void*);

	/*
	* Function to read symbol from elf
	*/
	int elf_get_symbol_value(elf_header* hdr, int symbol_table_index, uint16_t symbol_index){
		if(table == SHN_UNDEF || index == SHN_UNDEF) return 0;
		elf_section_header* symtab = elf_section(hdr, table);
	 
		uint32_t symtab_entries = symtab->sh_size / symtab->sh_entsize;
		if(index >= symtab_entries) {
			ERROR("Symbol Index out of Range (%d:%u).\n", table, index);
			return ELF_RELOC_ERR;
		}
	 
		int symaddr = (int)hdr + symtab->sh_offset;
		Elf32_Sym *symbol = &((Elf32_Sym *)symaddr)[index];
		if(symbol->st_shndx == SHN_UNDEF) {
			// External symbol, lookup value
			Elf32_Shdr *strtab = elf_section(hdr, symtab->sh_link);
			const char *name = (const char *)hdr + strtab->sh_offset + symbol->st_name;
	 
			extern void *elf_lookup_symbol(const char *name);
			void *target = elf_lookup_symbol(name);
	 
			if(target == NULL) {
				// Extern symbol not found
				if(ELF32_ST_BIND(symbol->st_info) & STB_WEAK) {
					// Weak symbol initialized as 0
					return 0;
				} else {
					ERROR("Undefined External Symbol : %s.\n", name);
					return ELF_RELOC_ERR;
				}
			} else {
				return (int)target;
			}
		} else if(symbol->st_shndx == SHN_ABS) {
			// Absolute symbol
			return symbol->st_value;
		} else {
			// Internally defined symbol
			Elf32_Shdr *target = elf_section(hdr, symbol->st_shndx);
			return (int)hdr + symbol->st_value + target->sh_offset;
		}
	}

};

#endif //ELF_H