/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines the output stream used internaly by the kernel.           *
* Currently only streaming to the video class for screen output.              *
*******************************************************************************/
#include "cerberus_console.h"

namespace internal_stream{
	/*
	* cerberus_console_color init
	* takes foreground and background codes, sets col to correct code
	*/
	cerberus_console_color::cerberus_console_color(uint8_t fore = 0x7, uint8_t back = 0x0){
		color = ((back << 4) & 0x0f) | (fore & 0x0f);
	}

	/*
	* cerberus_console init
	*/
	cerberus_console::cerberus_console(){
		attr = 0x7;
	}

	/*
	* cerberus_console init
	* Set default color at init
	*/
	cerberus_console::cerberus_console(uint8_t fore, uint8_t back){
		attr = ((back << 4) & 0x0f) | (fore & 0x0f);
	}

	//----------------------------------------;
	// Stream manipulators and utilities      ;
	//                                        ;
	//----------------------------------------;

	/*
	* flush n lines back in y direction from current
	* cursor possition
	* bool whiteout if true flushes with whitespace
	* else expects to over write with next stream
	* operator and just resets possition
	*/
	void cerberus_console::flush(uint8_t n, bool whiteout){
		uint16_t current_pos = video::get_cursor_y();
		if(current_pos - n < 0)	//don't leave screen buffer
			n = current_pos;
		video::set_cursor_y(current_pos - n);
		video::set_cursor_x(0);
		if(whiteout){
			//not implemented
		}
		video::update_cursor();
	}

	//----------------------------------------;
	// Overloaded stream operators            ;
	//                                        ;
	//----------------------------------------;

	/*
	* output a character
	*/
	cerberus_console& cerberus_console::operator <<(char c){
		uint8_t temp = video::get_attributes();
		video::set_attributes(attr);
		video::putch(c);
		video::set_attributes(temp);
		return *this;
	}

	/*
	* output a string
	*/
	cerberus_console& cerberus_console::operator <<(char *str){
		uint8_t temp = video::get_attributes();
		video::set_attributes(attr);
		video::puts(str);
		video::set_attributes(temp);
		return *this;
	}

	/*
	* output a string
	*/
	cerberus_console& cerberus_console::operator <<(const char *str){
		uint8_t temp = video::get_attributes();
		video::set_attributes(attr);
		video::puts((char *)str);
		video::set_attributes(temp);
		return *this;
	}

	/*
	* output a uint32
	*/
	cerberus_console& cerberus_console::operator <<(uint32_t n){
		uint8_t temp = video::get_attributes();
		video::set_attributes(attr);
		video::puts(convert(n,10));
		video::set_attributes(temp);
		return *this;
	}

	/*
	* output a int32
	*/
	cerberus_console& cerberus_console::operator <<(int32_t  n){
		uint8_t temp = video::get_attributes();
		video::set_attributes(attr);
		if(n < 0){
			video::putch('-');
			n *= -1;
		}
		video::puts(convert(n,10));
		video::set_attributes(temp);
		return *this;
	}

	/*
	* output a float
	*/
	cerberus_console& cerberus_console::operator <<(float n){
		uint8_t temp = video::get_attributes();
		video::set_attributes(attr);
		video::puts(ftoa(n));
		video::set_attributes(temp);
		return *this;
	}

	/*
	* set color of stream
	*/
	cerberus_console& cerberus_console::operator <<(cerberus_console_color k){
		attr = k.color;
		return *this;
	}
};