/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines basic video output from kernel space.                     *
* It is conmmon to have VGA support built in to hardware even as just a       *
* reliable fallback so we can use it to write to the screen from kernel       *
* without more advanced drivers.                                              *
*                                                                             *
* In the future we may wish to move this out of kernel space and into an      *
* OSLib but then we can't get output from kernel easily.                      *
*******************************************************************************/
#ifndef VIDEO_H
#define VIDEO_H

#include "types.h"
#include "ports.h"
#include "utils.h"

// VGA color codes
#define VGA_BLACK         0
#define VGA_BLUE          1
#define VGA_GREEN         2
#define VGA_CYAN          3
#define VGA_RED           4
#define VGA_MAGENTA       5
#define VGA_BROWN         6
#define VGA_LIGHT_GREY    7

#define VGA_GREY          8
#define VGA_LIGHT_BLUE    9
#define VGA_LIGHT_GREEN   10
#define VGA_LIGHT_CYAN    11
#define VGA_LIGHT_RED     12
#define VGA_LIGHT_MAGENTA 13
#define VGA_LIGHT_BROWN   14
#define VGA_WHITE         15

/*
* Functionality to draw the kernel consol on the default
* VGA monitar
*/
namespace video{
	void init();

	void putch(char);
	void puts(char*);
	void puts(const char*);

	void hex(uint32_t);
	void dec(uint32_t);

	void clear();
	void scroll();

	void set_attributes(uint8_t, uint8_t);
	void set_attributes(uint8_t);
	uint8_t get_attributes();
	uint16_t get_cursor_x();
	uint16_t get_cursor_y();
	void set_cursor_x(uint16_t);
	void set_cursor_y(uint16_t);

	void update_cursor();
};

#endif //VIDEO_H
