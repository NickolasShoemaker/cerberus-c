/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* The memory pool is the logic for allocating memory from the heap. We        *
*******************************************************************************/
#include "memory_pool.h"

#define BLOCK_SIZE(x) (x + sizeof(mem_header_t) + sizeof(mem_footer_t))

/*
* Create a memory pool startin at s and ending at e
*/
memory_pool::memory_pool(uint8_t* s,uint8_t* e){
	start = pos = s;
	end = e;

	used = 0;
	root.address = 0;
	root.size = 0;
	root.is_hole = false;

	root.magic = MEM_MAGIC;

	root.next = NULL;
	root.prev = NULL;
	
	error = 0;
}

/*
* Get amount of used space in bytes including memory header and footer
*/
uint32_t memory_pool::get_used(){
	return used;
}

/*
* Get amount of unused space in bytes
*/
uint32_t memory_pool::get_unused(){
	return get_size() - used;
}

/*
* Get size of heap in bytes
*/
uint32_t memory_pool::get_size(){
	return (uint32_t)(end - start);
}

/*
* Allocate memory from heap and return address for use
*/
char* memory_pool::alloc(uint32_t n){
	if(n == 0){
		error = POOL_NO_ERROR;
		return NULL;	
	}
	
	mem_header_t* mem_temp = smallest_hole(n);
	if(mem_temp != NULL){ 
		mem_footer_t* foot_temp = (mem_footer_t*)(mem_temp->address + mem_temp->size);
		if(mem_temp->magic != MEM_MAGIC || foot_temp->magic != MEM_MAGIC){
			error = POOL_BREACH;
			return NULL;
		}
		mem_temp->is_hole = false;
		
		used += BLOCK_SIZE(n); // update the amount of used memory

		if(mem_temp->size == n){
			return (char*)mem_temp->address;
		}
		foot_temp = (mem_footer_t*)(mem_temp->address + n);
		foot_temp->head = mem_temp;
		foot_temp->magic = MEM_MAGIC;
		return (char*)mem_temp->address;
	}

	if((signed)BLOCK_SIZE(n) > (end - pos)){
		error = POOL_OUT_OF_MEMORY;
		return NULL;
	}

	used += BLOCK_SIZE(n); // update the amount of used memory

	mem_header_t* block = (mem_header_t*)pos;
	block->address = (char*)pos + sizeof(mem_header_t);
	block->size = n;
	block->is_hole = 0;		// is not hole
	block->magic = MEM_MAGIC;

	mem_footer_t* foot = (mem_footer_t*)(pos + sizeof(mem_header_t) + n);
	
	foot->magic = MEM_MAGIC;
	foot->head = block;

	pos += BLOCK_SIZE(n);
	
	mem_header_t* t = &root;	// pointer to tree
	while(t->next) t = t->next;	// move to end of memory

	// add to queue
	t->next = block;	// add block to end of queue
	block->prev = t;	// set previous to t
	block->next = NULL;	// next is null
	return (char*)block->address;
}

/*
* Return memory to the heap
*/
void memory_pool::free(char* mem){
	mem_header_t* block = (mem_header_t*)((uint32_t)(mem) - sizeof(mem_header_t));
	mem_footer_t* foot = (mem_footer_t*)(block->address + block->size);

	// check memory sanity	
	if(block->magic != MEM_MAGIC || foot->magic != MEM_MAGIC){
		error = POOL_BREACH;
		return;
	}
	used -= BLOCK_SIZE(block->size);

	block->is_hole = true;	// memory is now hole

	// prepare to add node to hole tree
	mem_header_t* prev = block->prev;
	mem_header_t* next = block->next;
	
	if(prev && prev->is_hole){ // if previous is not null and is hole
		foot->head = prev; // set prev as the head of the foot
		prev->size = (uint32_t)foot - (uint32_t)prev + sizeof(mem_header_t);
	}
	
	error = POOL_NO_ERROR;
}

/*
* Find the smallest free hole in list that is larger than or equal to size
*/
mem_header_t* memory_pool::smallest_hole(uint32_t size){
	if(!root.next) return NULL;	                    // if tree has no holes
	mem_header_t* suitable = NULL;	                // best hole for allocation
	mem_header_t* iter = &root;	                    // start iter at root node

	while((iter = iter->next) != 0){	            // while not end of tree
		if(iter->size >= size) {		            // node size >= requested size
			if(suitable == NULL && iter->is_hole){	// set first option
				suitable = iter;
				continue;
			}else if(iter->size < suitable->size && iter->is_hole){// if node is more suitable, set suitable
				suitable = iter;
				continue;
			}
		}
	}
	
	return suitable;		// return best hole
}

/*
* Get error if there is one
*/
uint8_t memory_pool::get_error(){
	return error;
}