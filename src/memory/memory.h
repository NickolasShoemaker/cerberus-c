/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file declares the memory handling functionality for the Cerberus       *
* kernel. Since we are the kernel we need to handle both our own memory       *
* and we need to distribute memory to processes.                              *
*******************************************************************************/
#ifndef MEMORY_H
#define MEMORY_H

#include "memory_pool.h"

#include "multiboot.h"
#include "types.h"
#include "cerberus_console.h"
#include "bitmap.h"

namespace memory{
	void init();
	size_t used();
	size_t total();
	size_t left();

	void* alloc(size_t);
	void free(void*);

	void* alloc_frame();	
	void free_frame(void*);

	size_t kernel_used();
	size_t kernel_total();
};

//Prototypes for global memory operators so we can handle them
void* 	operator new		(size_t);
void* 	operator new	[]	(size_t);
void  	operator delete		(void*);
void  	operator delete	[]	(void*);

inline void* operator new		(size_t, void* p)   throw() { return p; }
inline void* operator new	[]	(size_t, void* p)   throw() { return p; }
inline void  operator delete  	(void*, void*) 		throw() { };
inline void  operator delete[]	(void*, void*) 		throw() { };

#endif //MEMORY_H
