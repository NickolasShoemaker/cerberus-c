/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines the memory handling functionality for the Cerberus        *
* kernel.                                                                     *
*******************************************************************************/
#include "memory.h"

// A token we use to map memory to
extern "C" uint32_t __end;

#define KERNEL_POOL_SIZE 0x800000 // that is 8MBs

namespace memory{

	size_t total_memory;
	size_t memory_pool_end;

	memory_pool mem((uint8_t*)&__end, (uint8_t *)((size_t)(&__end) + KERNEL_POOL_SIZE));	//create heap
	bitmap* pages;		//pointer to pages
	
	/*
	* Set the page table and prepare to handle memory
	*/
	void init(){
		//uint32_t* mem_upper = (uint32_t*)&mbi;
		//mem_upper += 3;
		total_memory = multiboot::multiboot_info_avail_mem_from_bios_mem_upper * 1024 + 0x100000;
		//total_memory = 0;
		memory_pool_end = ((size_t)(&__end) + KERNEL_POOL_SIZE);

		// The first thing we do is allocate the page table for the memory
		pages = new bitmap(total_memory / 0x1000);
		for(size_t i = 0; i < memory_pool_end / 0x1000; i++){
			pages->set(i,true);
		}
	}
	
	/*
	* Get number of bytes used in kernel heap
	*/
	size_t kernel_used(){
		return mem.get_used();
	}
	
	/*
	* Get size of kernel heap
	*/
	size_t kernel_total(){
		return mem.get_size();
	}
	
	/*
	* Number of used pages, number of used bits in page table times size of table
	*/
	size_t used(){
		return pages->get_used() * 0x1000;
	}
	
	/*
	* The physical size in bytes used by the page tables
	*/
	size_t total(){
		return  pages->get_size() * 0x1000;
	}
	
	/*
	* How much free memory is left
	*/
	size_t left(){
		return total() - used();
	}
	
	/*
	* Our memory allocator for the heap
	* make sure we don't end up allocating before this is defined
	*/
	void* alloc(size_t s){
		void* addr = mem.alloc(s);
		if(addr == NULL){
			switch(mem.get_error()){
				case 0:
					break;
				case 1:
					internal_stream::kerror << "Alloc: Memory has lost integrity.\n";
					break;
				case 2:
					internal_stream::kerror << "Alloc: Memory has run out for the kernel.\n";
					break;
				default:
					break;

			};
		}
		return addr;
	}
	
	/*
	* Allocate an entire page
	*/
	void* alloc_frame(){
		size_t empty = pages->find_first_empty();
		pages->set(empty,1);
		void* p = (void*)(empty * 0x1000);
		return p;
	}
	
	/*
	* Free an entire page
	*/
	void free_frame(void* p){
		size_t frame = (size_t)(p) / 0x1000;
		pages->set(frame,0);
	}
	
	/*
	* Return memory to the heap
	*/	
	void free(void* addr){

		mem.free((char*)addr);	

		if(mem.get_error()){
			switch(mem.get_error()){
				case 1:
					internal_stream::kerror << "Free: Memory has lost integrity.\n";
					break;
				case 2:
					internal_stream::kerror << "Free: Memory has run out for the kernel.\n";
					break;
				default:
					break;

			};
		}
	}
};

//redirect global operators to memory class
void* operator new	 	(size_t size)	{return memory::alloc(size);} 
void* operator new	 []	(size_t size)	{return memory::alloc(size);} 
void  operator delete	(void* p)		{memory::free(p);} 
void  operator delete[]	(void* p)		{memory::free(p);}