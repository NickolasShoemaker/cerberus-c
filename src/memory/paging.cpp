/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines the functionality for kernel paging tables.               *
*                                                                             *
*******************************************************************************/
#include "paging.h"

namespace paging{
	/*
	* Save CPU flag state, disable interrupts, then restore flags
	*/
	void init(){
		uint32_t e_temp = read_eflags();
		interrupts::disable();
		write_eflags(e_temp);
	}

	/*
	* Read command register and set paging enabled bit
	* while keeping the rest the same
	*/
	void enable(){
		write_cr0(read_cr0() | 0x80000000);
	}

	/*
	* Read command register and set paging disabled bit
	* while keeping the rest the same
	*/
	void disable(){
	    write_cr0(read_cr0() & ~0x80000000);
	}
};