/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* The memory pool is the logic for allocating memory from the heap. We        *
* maintain a doubly linked list of nodes to keep track of used and unused     *
* memory.                                                                     *
*******************************************************************************/
#ifndef MEMORY_POOL
#define MEMORY_POOL

#include "types.h"

#define MEM_MAGIC 0xDEADC0DE
#define POOL_NO_ERROR 0
#define POOL_BREACH 1
#define POOL_OUT_OF_MEMORY 2

/*
* Define structure of memory blocks for our heap
*/
extern "C"{
	typedef struct mem_header{
		char* address;
		uint32_t magic;
		size_t size;
		bool is_hole;

		struct mem_header* next;
		struct mem_header* prev;
	} __attribute__((packed)) mem_header_t;


	typedef struct mem_footer{
		mem_header_t* head;
		uint32_t magic;
	} __attribute__((packed)) mem_footer_t;
};

/*
* Class for the memory pool
*/
class memory_pool{
	mem_header_t root;

	uint8_t* start;
	uint8_t* end;
	uint8_t* pos;

	uint32_t used;
	uint8_t error;
public:
	memory_pool(uint8_t*,uint8_t*);

	char* alloc(uint32_t);
	void free(char*);

	uint32_t get_used();
	uint32_t get_unused();
	uint32_t get_size();
	uint8_t get_error();

	mem_header_t* smallest_hole(uint32_t);
};

#endif //MEMORY_POOL
