/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file declares paging tables used for memory                            *
*                                                                             *
*******************************************************************************/
#ifndef PAGING_H
#define PAGING_H

#include "types.h"
#include "isr.h"
#include "utils.h"

/*
* Page Descriptor Table
*/
struct PDT{
	uint32_t entries[1024]; //4096 bytes per page, 32 bits * 1024
}__attribute__ ((packed));

/*
* Page Descriptor
*/
struct PD{
	PDT* tables[1024];
	uint32_t phys_tables[1024];
	uint32_t phys_addr;
}__attribute__ ((packed));

/*
* Paging logic
*/
namespace paging{
	void init();
	void enable();
	void disable();
};

#endif //PAGING_H
