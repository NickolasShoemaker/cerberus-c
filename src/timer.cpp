/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file holds the timer functionality for the Cerberus kernel.            *
*******************************************************************************/
#include "timer.h"

namespace timer{
	/*
	* Initialized the timer
	*/
	void init(){
		internal_stream::kout << "Initializing timer.\n";

		uint32_t frequency = TICKS_PER_SEC;
		uint32_t divisor = 1193180 / frequency;
		outb(0x43, 0x36);

		uint8_t l = (uint8_t)(divisor & 0xFF);
		uint8_t h = (uint8_t)((divisor >> 8) & 0xFF);

		outb(0x40, l);
		outb(0x40, h);
	}
};
