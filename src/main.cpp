/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* Cerberus is aimed at getting as close to the microkernel exokernel          *
* architecture as possible which is a collection of moduals or OSLibs         *
* that can be swaped in and out to change the functionality of the OS         *
* without sacrificing stability or speed. The kernel will distribute          *
* resources to the OSLibs which will be responsible for them with no          *
* virtualization.                                                             *
*******************************************************************************/

#include "video.h"
#include "timer.h"
#include "isr.h"
#include "multiboot.h"
#include "cerberus_console.h"
#include "memory.h"
#include "utils.h"
#include "cpuid.h"

// We should move this somewhere else
extern "C" uint32_t magic;

/*
* Main Function of Cerberus
* This is called in boot.s after the kernel is done booting and setting up
* the c++ state
*/
int main(void){
	video::init();
	video::clear();

	// Create a console color object for intraduction message
	internal_stream::cerberus_console_color blue(VGA_BLUE, VGA_BLACK);
	internal_stream::kout << blue << "Welcome to Cerberus a microkernel exokernel OS.\n";
	internal_stream::kout << internal_stream::cerberus_console_color(0x7,0x0);

	multiboot::init();
	//multiboot::print_mboot_header();

	// set up timer, interrupts, memory managment
	timer::init();
	interrupts::enable();
	memory::init();

	internal_stream::kout << "Kernel Memory:\n";
	internal_stream::kout << "	Total:  " << memory::kernel_total() << "\n";
	internal_stream::kout << "	Used:   " << memory::kernel_used() << "\n";
	double mem_used = ((double)memory::kernel_used() / (double)memory::kernel_total());
	internal_stream::kout << "            " << (float)mem_used << "%\n\n";

	internal_stream::kout << "System Memory:\n";
	internal_stream::kout << "	Total: " << memory::total() << "\n";
	internal_stream::kout << "	Used:  " << memory::used() << "\n";
	internal_stream::kout << "           " << (float)memory::used() / (float)memory::total() << "%\n";
	
	// use CPUID to learn more about this system
	bool cpuid_avail = check_cpuid();
	if(cpuid_avail){
		internal_stream::kout << "cpuid found\n";
	}else{
		internal_stream::kerror << "cpuid not found\n";
	}

	// test the error message
	internal_stream::kerror << "A test error has occured! Don't Panic.\n";
	internal_stream::kout << "But this is normal output\n";
	//internal_stream::kout.flush(1);

	return 0;
}
