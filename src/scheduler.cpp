/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
* This defines the logic for the task scheduler.                              *
*******************************************************************************/
#include "scheduler.h"

/*
* Simple schedualer we passing in an address space for the task
* but we have to tasks to call yet
* Called in idt.s
*/
extern "C" uint32_t schedule(uint32_t stack){
	outb(0xe9, 'M');
	system_ticks++;
	if(!tasking) return stack;
	return stack;
	/*
	current_task->esp0 = stack;
	do{
		
		if(current_task->next){
			if(current_task->next->destroy_me)
				destroy_task(current_task->next);
			if(current_task->next)
				current_task =  current_task->next;
			else
				current_task = ready;
		}else
			current_task = ready;
			
		if (current_task->waiting_sem )
			if (current_task->waiting_sem->n == 0)
				continue;
	}while(current_task->sleep > system_ticks);
	
	if(current_task->magic != 0xDEADC0DE){
		kprintf("Corrupted task!!\n");
		for(;;);
	}
	write_cr3(current_task->dir->phys_tables);
	//TSS32.esp0 = current_task->esp0;
	return current_task->esp0;*/
};