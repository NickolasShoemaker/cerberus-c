/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This is the process sceduler. Currently this is very simple but we can      *
* improve it with priority multilevel queues.                                 *
*******************************************************************************/
#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "types.h"
#include "video.h"
#include "utils.h"
#include "ports.h"

uint32_t system_ticks = 0;
bool tasking = false;

/*
* This is a WIP so ignor for now
*/
#if 0
#include "queue.hpp"

namespace scheduler{
	uint32_t system_ticks = 0;
	bool tasking = false;

	delta_queue sleep_queue(50);
	queue<task*> active_queue(100);
	task* current_active;

	//prototypes
	void init();
	uint32_t ticks();

	struct sleep_node{
		task* sleeping_task;
		uint32_t delay;
	};

	class delta_queue{
	public:
		/*
		* create delta_queue as a subtype of queue
		*/
		delta_queue(int size):queue<sleep_node>(size){}
		/*
		* return true if element should be dequeued
		* else decrease delay timer
		*/
		bool tick(){
			sleep_node* temp = peek();
			if(temp->delay == 0){
				return true;
			}else{
				(temp->delay)--;
				return false;
			}
		}
		void insert(sleep_node* sn){
			if(isFull()){
				resize(getSize() + 10);
			}

			if(isEmpty()){
				enqueue(*sn);
				return;
			}

			//move head back 1
			if(head_index == 0){
				head_index == max_queue_size;
			}else{
				head_index--;
			}

			array[(index = head_index)] = *sn;
			int next;

			//ripple
			for(;;){
				if(index + 1 > max_queue_size){
					next = 0;
				}else{
					next = index + 1;
				}

				if(array[index].delay > array[next].delay){
					array[index].delay -= array[next].delay;	//decrease time

					sleep_node temp = array[next];
					array[next] = array[index];
					array[index] = temp;
				}else{
					return;
				}

				index = next;
			}
		}
	};
};

#endif //if 0

#endif //SCHEDULER_H
