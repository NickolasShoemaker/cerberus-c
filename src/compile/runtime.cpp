/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file contains functions and logic to ensure the compiler produces      *
* the correct results for our code.                                           *
*                                                                             *
* C++ does some interesting things to work. Since we are compiling weird      *
* we are just helping it along.                                               *
*******************************************************************************/

extern "C"{

	/*
	* This function calls the constructors for global objects
	* C++ creates an array of function calls that are run at program start to handle class
	* objects that appear at global scope and thus don't have a chance to be constructed
	*/
	void __init_array(){
	
		extern void (*init_array_start)();		//create funtion pointer for array
			
		void (**cons)() = &init_array_start;	//set function pointer pointer to address of array
	
		int total = *(int *)cons;				//set cons as int pointer and dereference to int, total number of functions in array
		cons++;									//move to first function
		
		while(total){							//set loop to execute all instructions
			(*cons)();							//execute address as function
			total--;							//decriment total
			cons++;								//increment to next function
		}
	}


	/*
	* This is a support rutine that is called if a pure virtual function can not be called
	* This should be imposible
	*/
	void __cxa_pure_virtual(){
	    // We just have this here so gcc doesn't complain.
	}
}