/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file holds the declarations for the timer functionality.               *
*******************************************************************************/
#ifndef TIMER_H
#define TIMER_H

#include "types.h"
#include "ports.h"
#include "cerberus_console.h"

// set the number of ticks per second
#define TICKS_PER_SEC 20

namespace timer{
	void init();
};

#endif //TIMER_H
