/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file handles getting advanced boot information from a boot loader such *
* as grub.                                                                    *
******************************************************************************/
#include "multiboot.h"

#include "cerberus_console.h"

namespace multiboot{

	/*
	* Populate multiboot structure with information provided by grub
	*/
	void init(){
		multiboot_header_magic										= *(mboot + 1);
		multiboot_header_feature_flags								= *(mboot + 2);
		multiboot_header_checksum									= *(mboot + 3);
		multiboot_header_header_addr								= *(mboot + 4);
		multiboot_header_load_addr									= *(mboot + 5);
		multiboot_header_load_end_addr								= *(mboot + 6);
		multiboot_header_bss_end_addr								= *(mboot + 7);
		multiboot_header_entry_addr									= *(mboot + 8);
		multiboot_header_mode_type									= *(mboot + 9);
		multiboot_header_width										= *(mboot + 10);
		multiboot_header_height										= *(mboot + 11);
		multiboot_header_depth										= *(mboot + 12);

		// read values from address starting at mbi populated by grub
	    multiboot_info_flags										= *(mbi + 1);
	    multiboot_info_avail_mem_from_bios_lower					= *(mbi + 2);
	    multiboot_info_avail_mem_from_bios_mem_upper				= *(mbi + 3);
	    multiboot_info_boot_device									= *(mbi + 4);
	    multiboot_info_cmdline										= *(mbi + 5);
	    multiboot_info_boot_module_count							= *(mbi + 6);
	    multiboot_info_boot_module_addr								= *(mbi + 7);
	    multiboot_info_multiboot_aout_symbol_table_tabsize			= *(mbi + 8);
	    multiboot_info_multiboot_aout_symbol_table_strsize			= *(mbi + 9);
	    multiboot_info_multiboot_aout_symbol_table_addr				= *(mbi + 10);
	    multiboot_info_multiboot_aout_symbol_table_reserved			= *(mbi + 11);
	    multiboot_info_multiboot_elf_section_header_table_num		= *(mbi + 12);
	    multiboot_info_multiboot_elf_section_header_table_size		= *(mbi + 13);
	    multiboot_info_multiboot_elf_section_header_table_addr		= *(mbi + 14);
	    multiboot_info_multiboot_elf_section_header_table_shndx		= *(mbi + 15);
	    multiboot_info_mmap_length									= *(mbi + 16);
	    multiboot_info_mmap_addr									= *(mbi + 17);
	    multiboot_info_drives_length								= *(mbi + 18);
	    multiboot_info_drives_addr									= *(mbi + 19);
	    multiboot_info_rom_config_table								= *(mbi + 20);
	    multiboot_info_boot_loader_name								= *(mbi + 21);
	    multiboot_info_apm_table									= *(mbi + 22);
	    multiboot_info_vbe_control_info								= *(mbi + 23);
	    multiboot_info_vbe_mode_info								= *(mbi + 24);
	    multiboot_info_vbe_mode										= *(uint16_t*)(&multiboot_info_vbe_mode_info +  1);
	    multiboot_info_vbe_interface_seg							= *(&multiboot_info_vbe_mode + 1);
	    multiboot_info_vbe_interface_off							= *(&multiboot_info_vbe_mode + 2);
	    multiboot_info_vbe_interface_len							= *(&multiboot_info_vbe_mode + 3);
	}

	/*
	* Print mboot header for debug purposes
	*/
	void print_mboot_header(){
		internal_stream::kout << "MBOOT at " << (uint32_t)mboot << "\n";
		internal_stream::kout << "multiboot_header_magic								";
		internal_stream::kout << multiboot_header_magic								 << "\n";
		internal_stream::kout << "multiboot_header_feature_flags 					    ";
		internal_stream::kout << multiboot_header_feature_flags 					 << "\n";
		internal_stream::kout << "multiboot_header_checksum					 		    ";
		internal_stream::kout << multiboot_header_checksum					 		 << "\n";
		internal_stream::kout << "multiboot_header_header_addr						    ";
		internal_stream::kout << multiboot_header_header_addr						 << "\n";
		internal_stream::kout << "multiboot_header_load_addr				 			";
		internal_stream::kout << multiboot_header_load_addr				 			 << "\n";
		internal_stream::kout << "multiboot_header_load_end_addr 					    ";
		internal_stream::kout << multiboot_header_load_end_addr 					 << "\n";
		internal_stream::kout << "multiboot_header_bss_end_addr	 					    ";
		internal_stream::kout << multiboot_header_bss_end_addr	 					 << "\n";
		internal_stream::kout << "multiboot_header_entry_addr			 			    ";
		internal_stream::kout << multiboot_header_entry_addr			 			 << "\n";
		internal_stream::kout << "multiboot_header_mode_type				            ";
		internal_stream::kout << multiboot_header_mode_type				             << "\n";
		internal_stream::kout << "multiboot_header_width								";
		internal_stream::kout << multiboot_header_width								 << "\n";
		internal_stream::kout << "multiboot_header_height							    ";
		internal_stream::kout << multiboot_header_height							 << "\n";
		internal_stream::kout << "multiboot_header_depth								";
		internal_stream::kout << multiboot_header_depth								 << "\n";
	}

	/*
	* Print mbi for debug purposes
	*/
	void print_mbi(){
		internal_stream::kout << "multiboot info table at " << (uint32_t)mbi << " \n";
		internal_stream::kout << "multiboot_info_flags									 	";
		internal_stream::kout << multiboot_info_flags									 << "\n";
		internal_stream::kout << "multiboot_info_avail_mem_from_bios_lower				 	";
		internal_stream::kout << multiboot_info_avail_mem_from_bios_lower				 << "\n";
		internal_stream::kout << "multiboot_info_avail_mem_from_bios_mem_upper			 	";
		internal_stream::kout << multiboot_info_avail_mem_from_bios_mem_upper			 << "\n";
		internal_stream::kout << "multiboot_info_boot_device								";
		internal_stream::kout << multiboot_info_boot_device								 << "\n";
		internal_stream::kout << "multiboot_info_cmdline									";
		internal_stream::kout << multiboot_info_cmdline									 << "\n";
		internal_stream::kout << "multiboot_info_boot_module_count						 	";
		internal_stream::kout << multiboot_info_boot_module_count						 << "\n";
		internal_stream::kout << "multiboot_info_boot_module_addr						 	";
		internal_stream::kout << multiboot_info_boot_module_addr						 << "\n";
		internal_stream::kout << "multiboot_info_multiboot_aout_symbol_table_tabsize		";
		internal_stream::kout << multiboot_info_multiboot_aout_symbol_table_tabsize		 << "\n";
		internal_stream::kout << "multiboot_info_multiboot_aout_symbol_table_strsize		";
		internal_stream::kout << multiboot_info_multiboot_aout_symbol_table_strsize		 << "\n";
		internal_stream::kout << "multiboot_info_multiboot_aout_symbol_table_addr		 	";
		internal_stream::kout << multiboot_info_multiboot_aout_symbol_table_addr		 << "\n";
		internal_stream::kout << "multiboot_info_multiboot_aout_symbol_table_reserved	 	";
		internal_stream::kout << multiboot_info_multiboot_aout_symbol_table_reserved	 << "\n";
		internal_stream::kout << "multiboot_info_multiboot_elf_section_header_table_num	 	";
		internal_stream::kout << multiboot_info_multiboot_elf_section_header_table_num	 << "\n";
		internal_stream::kout << "multiboot_info_multiboot_elf_section_header_table_size	";
		internal_stream::kout << multiboot_info_multiboot_elf_section_header_table_size	 << "\n";
		internal_stream::kout << "multiboot_info_multiboot_elf_section_header_table_addr	";
		internal_stream::kout << multiboot_info_multiboot_elf_section_header_table_addr	 << "\n";
		internal_stream::kout << "multiboot_info_multiboot_elf_section_header_table_shndx 	";
		internal_stream::kout << multiboot_info_multiboot_elf_section_header_table_shndx << "\n";
		internal_stream::kout << "multiboot_info_mmap_length								";
		internal_stream::kout << multiboot_info_mmap_length								 << "\n";
		internal_stream::kout << "multiboot_info_mmap_addr								 	";
		internal_stream::kout << multiboot_info_mmap_addr								 << "\n";
		internal_stream::kout << "multiboot_info_drives_length							 	";
		internal_stream::kout << multiboot_info_drives_length							 << "\n";
		internal_stream::kout << "multiboot_info_drives_addr								";
		internal_stream::kout << multiboot_info_drives_addr								 << "\n";
		internal_stream::kout << "multiboot_info_rom_config_table						 	";
		internal_stream::kout << multiboot_info_rom_config_table						 << "\n";
		internal_stream::kout << "multiboot_info_boot_loader_name						 	";
		internal_stream::kout << multiboot_info_boot_loader_name						 << "\n";
		internal_stream::kout << "multiboot_info_apm_table								 	";
		internal_stream::kout << multiboot_info_apm_table								 << "\n";
		internal_stream::kout << "multiboot_info_vbe_control_info						 	";
		internal_stream::kout << multiboot_info_vbe_control_info						 << "\n";
		internal_stream::kout << "multiboot_info_vbe_mode_info							 	";
		internal_stream::kout << multiboot_info_vbe_mode_info							 << "\n";
		internal_stream::kout << "multiboot_info_vbe_mode								    ";
		internal_stream::kout << multiboot_info_vbe_mode								 << "\n";
		internal_stream::kout << "multiboot_info_vbe_interface_seg 						    ";
		internal_stream::kout << multiboot_info_vbe_interface_seg 						 << "\n";
		internal_stream::kout << "multiboot_info_vbe_interface_off 						    ";
		internal_stream::kout << multiboot_info_vbe_interface_off 						 << "\n";
		internal_stream::kout << "multiboot_info_vbe_interface_len 						    ";
		internal_stream::kout << multiboot_info_vbe_interface_len 						 << "\n";
	}

	void print_mmap(){}

	/*
	* Print print everything for debug purposes
	*/
	void print(){
		print_mboot_header();
		print_mbi();
		print_mmap();
	}
};