/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file handles getting advanced boot information from a boot loader such *
* as grub.                                                                    *
******************************************************************************/
#ifndef MULTIBOOT_H
#define MULTIBOOT_H

#include "types.h"

#define MULTIBOOT_SEARCH                        8192        // How many bytes from the start of the file we search for the header.
#define MULTIBOOT_HEADER_MAGIC                  0x1BADB002  // The magic field should contain this.
#define MULTIBOOT_BOOTLOADER_MAGIC              0x2BADB002  // This should be in %eax.
#define MULTIBOOT_UNSUPPORTED                   0x0000fffc  // The bits in the required part of flags field we don't support.
#define MULTIBOOT_MOD_ALIGN                     0x00001000  // Alignment of multiboot modules.
#define MULTIBOOT_INFO_ALIGN                    0x00000004  // Alignment of the multiboot info structure.

/* Flags set in the 'flags' member of the multiboot header. */
#define MULTIBOOT_PAGE_ALIGN                    0x00000001  // Align all boot modules on i386 page (4KB) boundaries.
#define MULTIBOOT_MEMORY_INFO                   0x00000002  // Must pass memory information to OS.
#define MULTIBOOT_VIDEO_MODE                    0x00000004  // Must pass video information to OS.
#define MULTIBOOT_AOUT_KLUDGE                   0x00010000  // This flag indicates the use of the address fields in the header.

/* Flags to be set in the 'flags' member of the multiboot info structure. */
#define MULTIBOOT_INFO_MEMORY                   0x00000001  // is there basic lower/upper memory information?
#define MULTIBOOT_INFO_BOOTDEV                  0x00000002  // is there a boot device set?
#define MULTIBOOT_INFO_CMDLINE                  0x00000004  // is the command-line defined?
#define MULTIBOOT_INFO_MODS                     0x00000008  // are there modules to do something with?

/* These next two are mutually exclusive */
#define MULTIBOOT_INFO_AOUT_SYMS                0x00000010  // is there a symbol table loaded?
#define MULTIBOOT_INFO_ELF_SHDR                 0X00000020  // is there an ELF section header table?

#define MULTIBOOT_INFO_MEM_MAP                  0x00000040  // is there a full memory map?
#define MULTIBOOT_INFO_DRIVE_INFO               0x00000080  // Is there drive info?
#define MULTIBOOT_INFO_CONFIG_TABLE             0x00000100  // Is there a config table?
#define MULTIBOOT_INFO_BOOT_LOADER_NAME         0x00000200  // Is there a boot loader name?
#define MULTIBOOT_INFO_APM_TABLE                0x00000400  // Is there a APM table?
#define MULTIBOOT_INFO_VIDEO_INFO               0x00000800  // Is there video information?

#define MULTIBOOT_MEMORY_AVAILABLE              1
#define MULTIBOOT_MEMORY_RESERVED               2

namespace multiboot{

    extern "C" uint32_t* mbi;   //set in boot.s
    extern "C" uint32_t* mboot; //set in boot.s

    static uint32_t multiboot_header_magic; //MULTIBOOT_MAGIC
    static uint32_t multiboot_header_feature_flags;
    static uint32_t multiboot_header_checksum;  // The above fields plus this one must equal 0 mod 2^32.
    static uint32_t multiboot_header_header_addr;
    static uint32_t multiboot_header_load_addr;
    static uint32_t multiboot_header_load_end_addr;
    static uint32_t multiboot_header_bss_end_addr;
    static uint32_t multiboot_header_entry_addr;
    static uint32_t multiboot_header_mode_type;
    static uint32_t multiboot_header_width;
    static uint32_t multiboot_header_height;
    static uint32_t multiboot_header_depth;
    static uint32_t multiboot_info_flags;       // Multiboot info version number
    static uint32_t multiboot_info_avail_mem_from_bios_lower;
    static uint32_t multiboot_info_avail_mem_from_bios_mem_upper;
    static uint32_t multiboot_info_boot_device; // "root" partition
    static uint32_t multiboot_info_cmdline;     // Kernel command line
    static uint32_t multiboot_info_boot_module_count;
    static uint32_t multiboot_info_boot_module_addr;
    static uint32_t multiboot_info_multiboot_aout_symbol_table_tabsize;
    static uint32_t multiboot_info_multiboot_aout_symbol_table_strsize;
    static uint32_t multiboot_info_multiboot_aout_symbol_table_addr;
    static uint32_t multiboot_info_multiboot_aout_symbol_table_reserved;
    static uint32_t multiboot_info_multiboot_elf_section_header_table_num;
    static uint32_t multiboot_info_multiboot_elf_section_header_table_size;
    static uint32_t multiboot_info_multiboot_elf_section_header_table_addr;
    static uint32_t multiboot_info_multiboot_elf_section_header_table_shndx;
    static uint32_t multiboot_info_mmap_length;
    static uint32_t multiboot_info_mmap_addr;
    static uint32_t multiboot_info_drives_length;
    static uint32_t multiboot_info_drives_addr;
    static uint32_t multiboot_info_rom_config_table;
    static uint32_t multiboot_info_boot_loader_name;
    static uint32_t multiboot_info_apm_table;
    static uint32_t multiboot_info_vbe_control_info;
    static uint32_t multiboot_info_vbe_mode_info;
    static uint16_t multiboot_info_vbe_mode;
    static uint16_t multiboot_info_vbe_interface_seg;
    static uint16_t multiboot_info_vbe_interface_off;
    static uint16_t multiboot_info_vbe_interface_len;
    static uint32_t multiboot_mmap_entry_size;
    static uint64_t multiboot_mmap_entry_addr;
    static uint64_t multiboot_mmap_entry_len;
    static uint32_t multiboot_mmap_entry_type;
    static uint32_t multiboot_module_list_mod_start;
    static uint32_t multiboot_module_list_mod_end;
    static uint32_t multiboot_module_list_cmdline; // Module command line
    static uint32_t multiboot_module_list_pad;     // padding to take it to 16 bytes (must be zero)

    void init();
    void print_mboot_header();
    void print_mbi();
    void print_mmap();
    void print();
};

#endif // MULTIBOOT_H