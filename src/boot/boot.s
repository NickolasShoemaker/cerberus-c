;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cerberus an Operating System written in c++                               ;;
;; Nick Shoemaker                                                            ;;
;;                                                                           ;;
;; This boot entry is an attempt at making an entry into the kernel          ;;
;; that is all ready set up and sets up the things that grub does for        ;;
;; you (and more) but actually TELLS you about them!                         ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MBOOT_PAGE_ALIGN    equ 1<<0    ; Load kernel and modules on a page boundary
MBOOT_MEM_INFO      equ 1<<1    ; Provide your kernel with memory info
MBOOT_HEADER_MAGIC  equ 0x1BADB002 ; Multiboot Magic value, bootloader will have 0x2BADB002
; NOTE: We do not use MBOOT_AOUT_KLUDGE. It means that GRUB does not pass us a symbol table.
MBOOT_HEADER_FLAGS  equ MBOOT_PAGE_ALIGN | MBOOT_MEM_INFO
MBOOT_CHECKSUM      equ -(MBOOT_HEADER_MAGIC + MBOOT_HEADER_FLAGS)


[BITS 32]                       ; All instructions should be 32-bit.

; Data symbols called by mboot
[extern code]                   ; Start of the '.text' section.
[extern bss]                    ; Start of the .bss section.
[extern end]                    ; End of the last loadable section.

; Set a 4 byte alignment
align 4

;;
; multiboot header read by the bootloader
;;
[global mboot]                  ; Make 'mboot' accessible from C.
mboot:
    dd  MBOOT_HEADER_MAGIC      ; GRUB will search for this value on each
                                ; 4-byte boundary in your kernel file
    dd  MBOOT_HEADER_FLAGS      ; How GRUB should load your file / settings
    dd  MBOOT_CHECKSUM          ; To ensure that the above values are correct
    
    dd  mboot                   ; Location of this descriptor
    dd  code                    ; Start of kernel '.text' (code) section.
    dd  bss                     ; End of kernel '.data' section.
    dd  end                     ; End of kernel.
    dd  start                   ; Kernel entry point (initial EIP).

section .text

;; Function symbols called by start
[extern main]                   
[extern __init_array]
STACKSIZE equ 0x1000
[extern idt32_install]
[extern gdt32_install]

;;
; This is where we start executing code
; This is where the kernel actualy starts
;;
[global start]                   
start:
	cli                          ; clear interupts

    mov esp, stack + STACKSIZE   ; Set up are own stack so we know where it is
	
	mov [mbi], ebx               ; Load multiboot information into mbi
	mov [magic], eax             ; Push magic number to magic

	call gdt32_install           ; Install Global descriptor table
	call idt32_install           ; Install Interupt descriptor table
	
	call __init_array			; init global c++ objects
                            
    call main                   ; call our main() function.
    cli                         ; Disable interrupts.
    jmp $                       ; Enter an infinite loop, to stop the processor
                                ; executing whatever rubbish is in the memory
                                ; after our kernel!

[global STACK_SIZE]
STACK_SIZE: dd STACKSIZE
	
SECTION .bss
align 4096

;;
; Reserve space for our kernel stack
;;
[global stack]
stack: resb STACKSIZE

;;
; Reserve space for our user space stack
;;
[global user_stack]
user_stack: resb STACKSIZE

;;
; A magic number used for sanity checks
;;
[global magic]
magic: resd 1

;;
; global symbol for the multiboot_info structure
;;
[global mbi]
mbi: resd 1
