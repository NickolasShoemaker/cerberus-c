;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cerberus an Operating System written in c++                               ;;
;; Nick Shoemaker                                                            ;;
;;                                                                           ;;
;; This file defines utility functions in asm so that they may be            ;;
;; used elseware                                                             ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;----------------------------------------;
; Paging functions prototyped in utils.h ;
;                                        ;
;----------------------------------------;

;;
; Read the Extended Instruction Pointer
; This holds the address of the next instruction
; to be run
;;
[global read_eip]
read_eip:
	pop eax
	push eax
	ret
  
;;
; Read the control register to check
; the behavior of the processor;
;;
[global read_cr0]
read_cr0:
   mov eax, cr0
   retn

;;
; Write to the control register to change
; the behavior of the processor
;;
[global write_cr0]
write_cr0:
   push ebp
   mov ebp, esp
   mov eax, [ebp+8]
   mov cr0,  eax
   pop ebp
   retn

;;
; Write to command register 3 used for physical to virtual
; address translation and task switching.
; This is likely the address of the page table
;;
[global read_cr3]
read_cr3:
   mov eax, cr3
   retn

;;
; Write to command register 3 used for physical to virtual
; address translation and task switching.
;;
[global write_cr3]
write_cr3:
   push ebp
   mov ebp, esp
   mov eax, [ebp+8]
   mov cr3, eax
   pop ebp
   retn

;;
; Read the Extended flags register
; this register contains information about
; the processor state
;;
[global read_eflags]
read_eflags:
	pushf
	pop eax
	ret

;;
; Write the Extended flags register
; this register contains information about
; the processor state
;;
[global write_eflags]
write_eflags:
	push ebp
	mov ebp, esp
    mov eax, [ebp+8]
    push eax
    popf
    pop ebp
    retn

;----------------------------------------;
; Cpuid functions prototyped in cpuid.h  ;
;                                        ;
;----------------------------------------;

;;
; Determines if cpuid is supported
;
; returns 0 for false and 1 for true
;;
[global check_cpuid]
check_cpuid:
  pushfd                        ; push stack on eflags
  pushfd                        ;
  xor dword [esp], 0x00200000   ; xor cpuid bit on stack
  popfd                         ;
  pushfd                        ; move to see if bit changes
  pop eax                       ; pop into eax
  xor eax,[esp]
  popfd                         ; restore eflags
  and eax, 0x00200000           ; clear nonsense
  cmp eax, 0x00200000           ; check bit
  setz al                       ; set eax to true or false
  ret

;[global cpuid_get_vendor_string]
;cpuid_get_vendor_string:

