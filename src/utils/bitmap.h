/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file declares the mapping of bits being used by the system             *
*                                                                             *
*******************************************************************************/
#ifndef BITMAP_H
#define BITMAP_H

#include "types.h"
#include "utils.h"

class bitmap{
	size_t num_segs;
	size_t* segs;			//pointer to mapping of bits
	size_t num_used;		//number of 1's in mapping

public:
	bitmap(size_t);

	bool get(size_t);
	void set(size_t, bool);

	size_t get_size();
	size_t get_used();

	size_t find_first_empty();
};

#endif //BITMAP_H