/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* Header for basic string and memory functions that may be need in kernel     *
* space                                                                       *
*******************************************************************************/
#include "utils.h"

/*
* Copy len bytes from src to dest.
*/
void memcpy(uint8_t *dest, const uint8_t *src, uint32_t len){
    const uint8_t *sp = (const uint8_t *)src;
    uint8_t *dp = (uint8_t *)dest;
    for(; len != 0; len--) *dp++ = *sp++;
}

/*
* Write len copies of val into dest.
*/
void memset(uint8_t *dest, uint8_t val, uint32_t len){
    uint8_t *temp = (uint8_t *)dest;
    for ( ; len != 0; len--) *temp++ = val;
}

/*
* Compare two strings. Should return -1 if 
* str1 < str2, 0 if they are equal or 1 otherwise.
*/
int strcmp(char *str1, char *str2){
      int i = 0;
      int failed = 0;
      while(str1[i] != '\0' && str2[i] != '\0'){
          if(str1[i] != str2[i]){
              failed = 1;
              break;
          }
          i++;
      }
      // why did the loop exit?
      if( (str1[i] == '\0' && str2[i] != '\0') || (str1[i] != '\0' && str2[i] == '\0') )
          failed = 1;
  
      return failed;
}

/*
* Compare the first n digits of str1 and str2
*/
int strncmp(char* str1, char* str2, uint32_t n){
      unsigned int i = 0;
      int failed = 0;
      while(str1[i] != '\0' && str2[i] != '\0'){
          if(str1[i] != str2[i]){
              failed = 1;
              break;
          }
          i++;
          if(i == n){
          	return 0;
          }
      }
      // why did the loop exit?
      if( (str1[i] == '\0' && str2[i] != '\0') || (str1[i] != '\0' && str2[i] == '\0') )
          failed = 1;
  
      return failed;
}

/*
* Copy the NULL-terminated string src into dest, and
* return dest.
*/
char* strcpy(char* dest, const char* src){
	char* p = dest;
    do{
      *dest++ = *src++;
    }
    while (*src != 0);
    *dest = 0;
    return p;
}

/*
* Copy n characters from src to dest
*/
char* strncpy(char* dest, const char* src, uint32_t len){
	char* p = dest;
    do{
      *dest++ = *src++;
      len--;
    }
    while (*src != 0 && len);
    *dest = 0;
    return p;
}

/*
* Concatenate the NULL-terminated string src onto
* the end of dest, and return dest.
*/
/*
char* strcat(char* dest, const char* src){
    while (*dest != 0){
        *dest = *dest++;
    }

    do{
        *dest++ = *src++;
    }
    while (*src != 0);
    return dest;
}
*/

/*
* Count bytes from src to null terminator
*/
int strlen(char* src){
    int i = 0;
    while(*src++)
        i++;
    return i;
}

/*
* Change of base formula for up to hexadecimal
* can be increaded by adding digits after f.
*/
char* convert(uint64_t num, int base){
	static char buff[33];
	char* ptr;
	ptr=&buff[sizeof(buff)-1];
	*ptr='\0';
	do{
		*--ptr="0123456789abcdef"[num%base];  //array[index], index = num%base
		num/=base;
	}while(num!=0);
	
	return(ptr);
}

/*
* Convert a floating point into a string
*/
char* ftoa(float f){
	
	uint32_t x = *(uint32_t *)&f;
	
	static char fbuff[33];
	char* ptr = fbuff;
	
	unsigned int sign = x >> 31;
 	unsigned int exp = ((x >> 23) & 0xff) - 127;
 	unsigned int man = x & ((1 << 23) - 1);
 	man |= 1 << 23;
 	if (sign){
		*ptr = '-';
		ptr++;
	}
	char * m = convert(man >> (23 - exp), 10);
	strcpy(ptr, m);
	ptr += strlen(m);
 	
 	unsigned int frac = man & ((1 << (23-exp)) - 1);
 	unsigned int base = 1 << (23 - exp);

	if(frac != 0){
		*ptr = '.';
		ptr++;

	}
 	int c = 0;
 	while (frac != 0 && c++ < 6) {
  		frac *= 10;
 		m = convert((frac / base), 10);
		strcpy(ptr, m);
		ptr += strlen(m);
  		frac %= base;
 	}
	return fbuff;
 }
