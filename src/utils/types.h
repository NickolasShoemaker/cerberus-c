/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file declares various types and keyword macros for easier use in       *
* other areas of the kernel.                                                  *
*******************************************************************************/
#ifndef TYPES_H
#define TYPES_H

//Signed size types
typedef signed   char   int8_t;
typedef signed   short  int16_t;
typedef signed   int    int32_t;
typedef signed   long   int64_t;

//Unsigned size types
typedef unsigned char   uint8_t;
typedef unsigned short  uint16_t;
typedef unsigned int    uint32_t;
typedef unsigned long   uint64_t;

typedef __SIZE_TYPE__   size_t;
typedef void*           type_t;

/*
* I don't know if we are going to use any of these but here they are
*/
#define va_start(v,l) 	__builtin_va_start(v,l)
#define va_arg(v,l)   	__builtin_va_arg(v,l)
#define va_end(v)     	__builtin_va_end(v)
#define va_copy(d,s)  	__builtin_va_copy(d,s)
#define snprintf      	__builtin_snprintf
#define sprintf       	__builtin_sprintf

typedef __builtin_va_list va_list;

// Define some boolean macros
#undef NULL
#define NULL	0
#define TRUE 	1
#define FALSE 	0
#define true 	1
#define false 	0

/*
* Define registers
*/
typedef struct registers{
   uint32_t ds;               
   uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax; 
   uint32_t int_no, err_code; 
   uint32_t eip, cs, eflags, useresp, ss;
} registers_t;

// this is not a type, should be somewhere else
#define halt() 		asm volatile ("hlt")

// This should also be somewhere else
#ifndef asm
#define asm __asm__
#endif

#endif // TYPES_H
