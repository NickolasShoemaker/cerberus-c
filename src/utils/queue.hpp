/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines an Abstract Data Type for use in the kernel. A queue      *
* can be useful for schedualing and other low level logic.                    *
*                                                                             *
* It is best to keep this all as one file since it uses templates.            *
*******************************************************************************/
#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "cerberus_console.h"

template<typename Type>
class queue{
protected:
	int head_index;
	int tail_index;
	int max_queue_size;
	Type* array;
public:
	/*
	* queue default constructor                          
	* set index identifiers to illegal values, set max   
	* size and set up array                              
	*/
	queue(int size = 20){
		head_index = -1;
		tail_index = -1;
		max_queue_size = size;
		array = new Type [size];
	}

	/*
	* queue copy constructor                             
	* makes a deep copy of queue for function passing    
	*                                                    
	*/
	queue(queue& obj, int n = 0){
		if(n == 0){
			max_queue_size = obj.max_queue_size;
		}else{
			max_queue_size = n;
		}
		array = new Type [max_queue_size];

		head_index = 0;
		tail_index = getSize();

		//copy contents
		int j = obj.head_index;
		for(int i = 0; i < tail_index; i++){
			array[i] = obj.array[j++];
			if(j+1 == obj.max_queue_size)
				j = 0;
		}
	}

	/*
	* queue destructor                                   
	* deletes memory pointed to by array                              
	*/
	~queue(){
		delete[] array;
	}

	/*
	* trys to change queue to size n                     
	* returns new size of queue                                                              
	*/
	int resize(int n){
		old_array = array;
		old_size = max_queue_size;

		if(n == 0){
			return 0;			
		}else if(n <= max_queue_size){	//make sure we can shrink
			if(n < getSize()){
				max_queue_size = getSize();
			}else{
				max_queue_size = n;
			}
		}else{
			max_queue_size = n;
		}
		array = new Type [max_queue_size];

		//copy contents
		int j = head_index;
		tail_index = getSize();
		for(int i = 0; i < tail_index; i++){
			array[i] = old_array[j++];
			if(j+1 == old_size)
				j = 0;
		}
		head_index = 0;
		delete[] old_array;
		return max_queue_size;
	}

	/*
	* Returns number of objects currently in queue  
	*/
	int getSize(){
		int size = 0;
		if(obj.head_index > obj.tail_index){
			size = (obj.max_queue_size - obj.head_index) + obj.tail_index;
		}else{
			size = obj.tail_index  - obj.head_index;
		}
		return size;
	}

	/*
	* Returns number of object queue can hold in memory                                       
	*/
	int getCapacity(){
		return max_queue_size;
	}

	/*
	* isFull helper for queue                            
	*/
	bool isFull(){
		return (tail_index+1 == head_index || head_index == 0 && tail_index == max_queue_size);
	}

	/*
	* isEmpty helper for queue                           
	*/
	bool isEmpty(){
		return (tail_index == -1 && head_index == -1);
	}

	/*
	* enqueue wrapper for remove in queue                                     
	* Set the head data from add or print error          
	*/
	void enqueue(Type element){//0 <= index <= MAX_QUEUE_SIZE
		if(isFull()){
			internal_stream::kerror << "ERROR: Enqueueing a full queue.\n";
			//panic();
		}

		if(tail_index == -1 && head_index == -1){
			array[0] = element;
			head_index = tail_index = 0;
		}else if(tail_index == max_queue_size){
			array[(tail_index = 0)] = element;
		}else{
			array[++tail_index] = element;
		}
	}

	/*
	* dequeue wrapper for remove in queue                
	* Return the head data from remove or print error    
	*/
	Type dequeue(){
		int temp_index = head_index;
		if(isEmpty()){
			internal_stream::kerror << "ERROR: Dequeueing an empty queue.\n";
			//panic();
		}	

		if(head_index == tail_index){			//one element
			head_index = tail_index = -1;		//set to empty
		}else if(head_index == max_queue_size){	//need to loop
			head_index = 0;
		}else{
			head_index++;
		}

		return array[temp_index];
	}

	/*
	* get access to next item in queue without remove    
	* Return the head data from queue                    
	*/
	Type* peek(){
		return &array[head_index];
	}

	/*
	* Queue assignement operator                 
	* makes a deep copy of queue for function assigning                                                      
	*/
	queue& operator=(const queue & obj){
		head_index = obj.head_index;
		tail_index = obj.tail_index;
		max_queue_size = obj.max_queue_size;
		array = new Type [obj.max_queue_size];

		//copy contents
		for(int i=0;i<max_queue_size;i++){
			array[i] = obj.array[i];
		}
		return *this;
	}
};

#endif //QUEUE_HPP