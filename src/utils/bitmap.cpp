/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* This file defines the functionality the mapping of bits being used by       *
* the system                                                                  *
*******************************************************************************/
#include "bitmap.h"

/*
* Create and zero out a map of bits with size num
*/
bitmap::bitmap(size_t num){
	num_segs = num / sizeof(size_t) + (num % sizeof(size_t) ? 1 : 0);
	segs = new size_t[num_segs];
	num_used = 0;
	memset((uint8_t *) segs, 0, sizeof(size_t) * num_segs);	
}

/*
* Get boolean value of bit at bit_num
*/
bool bitmap::get(size_t bit_num){
	//return false instead of reading outside of mapping
	if(num_segs < bit_num / (sizeof(size_t) * 8)){
		return false;
	}
	
	return (segs[bit_num / (sizeof(size_t) * 8)] >> (bit_num % (sizeof(size_t) * 8))) & 1;
}

/*
* Set new value in mapping and update num_used to reflect number of 1's
*/
void bitmap::set(size_t bit_num, bool val){
	//return rather than writting outside the mapping
	if(num_segs < bit_num / (sizeof(size_t) * 8)){
		return;
	}

	bool past = get(bit_num);
	if(val){
		segs[bit_num / (sizeof(size_t) * 8)] |= (1) << (bit_num % (sizeof(size_t) * 8));
		if(!past)
			num_used++;
	}else{ 
		segs[bit_num / (sizeof(size_t) * 8)] &= ~((1) << (bit_num % (sizeof(size_t) * 8)));
		if(past)
			num_used--;
	}	
}

/*
* Get the size of the mapping of bits
*/
size_t bitmap::get_size(){
	return num_segs * sizeof(size_t);
}

/*
* Get the number of bits that are being used in the mapping
*/
size_t bitmap::get_used(){
	return num_used;
}

/*
* Sequentially move through bitmap until a zero bit is found
* return index of bit. This can be used as the arg to get()
* and set().
*/
size_t bitmap::find_first_empty(){
	size_t i = 0, j = 0;
	for(; i < num_segs; i++){
		if(segs[i] != 0xffffffff){
			j = 0;
			while((segs[i] >> j) & 0x1) j++;
			break;
		}
	}

	return i * (sizeof(size_t) * 8) + j;
}