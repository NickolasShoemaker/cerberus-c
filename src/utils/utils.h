/******************************************************************************
* Cerberus an Operating System written in c++                                 *
* Nick Shoemaker                                                              *
*                                                                             *
* Header for basic string and memory functions that may be need in kernel     *
* space                                                                       *
*******************************************************************************/
#ifndef UTILS_H
#define UTILS_H

#include "types.h"

// Function Prototypes
void memcpy(uint8_t*, const uint8_t*, uint32_t);
void memset(uint8_t*, uint8_t, uint32_t);
int strcmp(char*, char*);
int strncmp(char*, char*, uint32_t);
char* strcpy(char*, const char*);
char* strncpy(char*, const char*, uint32_t);
int strlen(char*);
char* convert(uint64_t, int);
char* ftoa(float f);
//char *strcat(char *dest, const char *src);

// Function prototypes for asm defined in utils.s
extern "C"{
	void write_cr0(uint32_t);
	void write_cr3(uint32_t);
	uint32_t write_eflags(uint32_t);
	uint32_t read_cr0();
	uint32_t read_cr3();
	uint32_t read_eip();
	uint32_t read_eflags();
};

#endif //UTILS_H
